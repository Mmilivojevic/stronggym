use stronggym;
CREATE TABLE korisnik (
id BIGINT AUTO_INCREMENT,
	korisnickoIme VARCHAR(30),
    lozinka VARCHAR(30) NOT NULL,
	email VARCHAR(20),
    ime VARCHAR(20),
    prezime VARCHAR(20),
	datumRodjenja DATE,
    adresa VARCHAR(30) NOT NULL,
    brojTelefona VARCHAR(20),
	datumRegistracije DATETIME,
	administrator BOOL DEFAULT false,
	PRIMARY KEY(id)
);
--drop table korisnik;
--ALTER TABLE korisnik 
--ADD id BIGINT AUTO_INCREMENT;
--ALTER TABLE korisnik MODIFY COLUMN email VARCHAR(50); 
--ALTER TABLE korisnik
--ADD block bool default false;
create table trening(
id BIGINT AUTO_INCREMENT,
	  naziv VARCHAR(30) NOT NULL,
	treneri VARCHAR(30) NOT NULL,
	 kratakOpis VARCHAR(10000) NOT NULL,
	cena decimal NOT NULL,
	vrstaTreninga ENUM('pojedinacni', 'grupni') DEFAULT 'pojedinacni',
	 nivoTreninga  ENUM('amaterski', 'srednji','napredni') DEFAULT 'amaterski',
	trajanjeTreninga decimal,
    
	PRIMARY KEY(id)

);


CREATE TABLE tipTreninga (
	id BIGINT AUTO_INCREMENT,
	naziv VARCHAR(25) NOT NULL,
    opis VARCHAR(1000) NOT NULL,
	PRIMARY KEY(id)
   
);
CREATE TABLE treningTipTreninga (
	treningId BIGINT,
    tipId BIGINT,
    PRIMARY KEY(treningId, tipId),
    FOREIGN KEY(treningId) REFERENCES trening(id)
		ON DELETE CASCADE,
    FOREIGN KEY(tipId) REFERENCES tipTreninga(id)
		ON DELETE CASCADE
   
);

insert into korisnik(id,korisnickoIme,lozinka,email,ime,prezime,datumRodjenja,adresa,brojTelefona,datumRegistracije,administrator) values (1,'mici','123','mici@gmial.com,','Milica','Milivojevic','2000-01-09','Novosadski 50','+08594562','2022-01-05',true);
insert into korisnik(id,korisnickoIme,lozinka,email,ime,prezime,datumRodjenja,adresa,brojTelefona,datumRegistracije,administrator) values (2,'miki','456','mira@gmial.com,','Mirjana','Zaric','1999-10-10','Novosadski 50','+08594562','2022-01-05',false);
insert into korisnik(id,korisnickoIme,lozinka,email,ime,prezime,datumRodjenja,adresa,brojTelefona,datumRegistracije,administrator) values (3,'du','789','du@gmial.com,','Dunja','Jazavac','2000-01-03','Novosadski 50','+08594562','2022-01-05',false);

insert into trening(id,naziv,treneri,kratakOpis,cena,vrstaTreninga,nivoTreninga,trajanjeTreninga) values (1,'aerobik','Mika Mikic,Vida Vidic','Uvod u dobru kondiciju,kardio trening sa vezbama izdrzljivosti','2000.00','grupni','srednji','45');
insert into trening(id,naziv,treneri,kratakOpis,cena,vrstaTreninga,nivoTreninga,trajanjeTreninga) values (2,'trening snage','Mika Mikic','Trening za one koji su vec u formi,za bolju stabilnost','2000.00','pojedinacni','napredni','45');
insert into trening(id,naziv,treneri,kratakOpis,cena,vrstaTreninga,nivoTreninga,trajanjeTreninga) values (3,'yoga','Mika Mikic,Vida Vidic','Opustanje uz yogu za naprednije','2500.00','grupni','napredni','45');
insert into tipTreninga(id,naziv,opis)values(1,'yoga','nesto');
insert into tipTreninga(id,naziv,opis)values(2,'aerobic','nesto');
insert into tipTreninga(id,naziv,opis)values(3,'snaga','nesto');

insert into treningTipTreninga(treningId,tipId)values(3,1);
insert into treningTipTreninga(treningId,tipId)values(2,3);
insert into treningTipTreninga(treningId,tipId)values(1,2);
CREATE TABLE slika (
	id BIGINT AUTO_INCREMENT,
	imagePath VARCHAR(25) NOT NULL,
	PRIMARY KEY(id)
   
);
CREATE TABLE treningSlika (
	treningId BIGINT,
    slikaId BIGINT,
    PRIMARY KEY(treningId, slikaId),
    FOREIGN KEY(treningId) REFERENCES trening(id)
		ON DELETE CASCADE,
    FOREIGN KEY(slikaId) REFERENCES slika(id)
		ON DELETE CASCADE
   
);
insert into slika(id,imagePath) values (1,'images/aerobik.jpg');
insert into slika(id,imagePath) values (2,'images/snaga.jpg');
insert into slika(id,imagePath) values (3,'images/yoga.jpg');
insert into slika(id,imagePath) values (4,'images/new.jpg');
insert into slika(id,imagePath) values (5,'images/new2.jpg');

insert into treningSlika(treningId,slikaId) values (1,1);
insert into treningSlika(treningId,slikaId) values (2,2);
insert into treningSlika(treningId,slikaId) values (3,3);
create table sala(
id BIGINT AUTO_INCREMENT,
	  oznaka VARCHAR(30) NOT NULL UNIQUE,
	kapacitet integer NOT NULL,
	zakazan bool DEFAULT false,
    
	PRIMARY KEY(id)

);
insert into sala (id,oznaka,kapacitet,zakazan) values(1,"IN",20,false);
insert into sala (id,oznaka,kapacitet,zakazan) values(2,"GR",20,true);
insert into sala (id,oznaka,kapacitet,zakazan) values(3,"AE",20,false);
create table termini(
	id BIGINT AUTO_INCREMENT,
    treningId BIGINT,
    salaId BIGINT,
    datum datetime not null,
     PRIMARY KEY(id),
    FOREIGN KEY(treningId) REFERENCES trening(id)
		ON DELETE CASCADE,
    FOREIGN KEY(salaId) REFERENCES sala(id)
		ON DELETE CASCADE
	
   
);
--SET FOREIGN_KEY_CHECKS=0; DROP TABLE termini;
--SET FOREIGN_KEY_CHECKS=1;
--drop table termini;
insert into termini(id,treningId,salaId,datum) values (1,1,1,'2021-01-10 10:15');
insert into termini(id,treningId,salaId,datum) values (2,2,3,'2021-01-10 10:15');

create table terminKorisnik(
	
    terminId BIGINT,
    korisnikId BIGINT,
   
     PRIMARY KEY(terminId,korisnikId),
    FOREIGN KEY(terminId) REFERENCES termini(id)
		ON DELETE CASCADE,
    FOREIGN KEY(korisnikId) REFERENCES korisnik(id)
		ON DELETE CASCADE
);
--drop table terminkorisnik;
insert into terminKorisnik(terminId,korisnikId) values (1,1);
insert into terminKorisnik(terminId,korisnikId) values (1,2);
insert into terminKorisnik(terminId,korisnikId) values (1,3);

insert into terminKorisnik(terminId,korisnikId) values (2,1);
insert into terminKorisnik(terminId,korisnikId) values (2,2);
insert into terminKorisnik(terminId,korisnikId) values (2,3);


CREATE TABLE listaZelja (
	id BIGINT AUTO_INCREMENT,
	treningId BIGINT NOT NULL,
    korisnikId BIGINT NOT NULL NOT NULL,
	PRIMARY KEY(id),
    FOREIGN KEY(treningId) REFERENCES trening(id)
		ON DELETE CASCADE,
	FOREIGN KEY(korisnikId) REFERENCES korisnik(id)
		ON DELETE CASCADE
);

INSERT INTO listaZelja (id,treningId, korisnikId) VALUES (1,1, 3);
INSERT INTO listaZelja (id,treningId, korisnikId) VALUES (2,2, 3);
INSERT INTO listaZelja (id,treningId, korisnikId) VALUES (3,3, 3);
CREATE TABLE komentar(
	komentarId BIGINT AUTO_INCREMENT,
    tekst VARCHAR(999) NOT NULL,
    ocena VARCHAR(30) NOT NULL,
    datumKomentara DATE,
    status ENUM('odobren', 'na cekanju','nije odobren') DEFAULT 'na cekanju',
    
    autorKomentara BIGINT NOT NULL,
	komentarisanTrening BIGINT NOT NULL,
    anoniman BOOL DEFAULT false,

	PRIMARY KEY(komentarId),
    FOREIGN KEY(komentarisanTrening) REFERENCES trening(id)
		ON DELETE CASCADE,
	FOREIGN KEY(autorKomentara) REFERENCES korisnik(id)
		ON DELETE CASCADE
);
INSERT INTO komentar(komentarId,tekst,ocena,datumKomentara,status,autorKomentara,komentarisanTrening,anoniman) VALUES(1,'neki tekst vezan za knjigu',"5",'2021-01-08',"na cekanju",3,1,true);
INSERT INTO komentar(komentarId,tekst,ocena,datumKomentara,status,autorKomentara,komentarisanTrening,anoniman) VALUES(2,'neki tekst vezan za knjigu',"4",'2021-01-08',"odobren",2,2,false);
INSERT INTO komentar(komentarId,tekst,ocena,datumKomentara,status,autorKomentara,komentarisanTrening,anoniman) VALUES(3,'neki tekst vezan za knjigu',"3",'2021-01-08',"na cekanju",3,3,false);

CREATE TABLE clanskaKartica(
	id BIGINT AUTO_INCREMENT,
    brojPoena int NOT NULL,
    popust VARCHAR(30) NOT NULL,
   
    korisnikId BIGINT NOT NULL,

    odobren BOOL DEFAULT false,

	PRIMARY KEY(id),
    FOREIGN KEY(korisnikId) REFERENCES korisnik(id)
		ON DELETE CASCADE
);
insert into clanskaKartica(id,brojPoena,popust,korisnikId,odobren) values(1,0,"5%",2,false);
insert into clanskaKartica(id,brojPoena,popust,korisnikId,odobren) values(2,0,"5%",2,false);
insert into clanskaKartica(id,brojPoena,popust,korisnikId,odobren) values(3,0,"5%",2,false);


package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.Zelje;

public interface ZeljeDAO {
	
	public List<Zelje> findAll(Long korisnikId);
	public int save(Zelje zelje);
	public int delete(Long zeljeId);
	public Zelje findOne(Long zeljeId);
	public int delete(Long terminId,Long korisnikId);

}

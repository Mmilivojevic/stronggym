package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.Termin;

public interface TerminDAO {
	public Termin findOne(Long id);
	public 	List<Termin> findAll();
	public void save(Termin termin);
	public void update(Termin termin);
	public void delete(Long id);
	public List<Termin> findTermineZaTrening(Long treningId);
	public List<Termin> findTermineZaKorisnika(Long korisnikId);
}

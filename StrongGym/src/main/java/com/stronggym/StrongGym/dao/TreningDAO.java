package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.Trening;

public interface TreningDAO {
//	public List<Trening >find( String naziv, String treneri, String kratakOpis, String slika, double cena, String vrstaTreninga,
//			String nivoTreninga, String trajanjeTreninga);
//
	public List<Trening> find(String naziv, Long tipId, Double cenaOd, Double cenaDo, String treneri, String vrstaTreninga, String nivoTreninga);
	
	public Trening findOne(Long id);
	public int save (Trening trening);
	public int update (Trening trening);
	public List<Trening> findAll();
}

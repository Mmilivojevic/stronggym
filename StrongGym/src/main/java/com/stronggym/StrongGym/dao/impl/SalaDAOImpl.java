package com.stronggym.StrongGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stronggym.StrongGym.dao.SalaDAO;

import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Sala;

@Repository
public class SalaDAOImpl implements SalaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class SalaRowMapper implements RowMapper<Sala> {
		
		@Override
		public Sala mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index=1;
			Long id=rs.getLong(index++);
			String oznaka=rs.getString(index++);
			int kapacitet=rs.getInt(index++);
			Boolean zakazan=rs.getBoolean(index++);
			
			Sala sala=new Sala(id, oznaka, kapacitet, zakazan);
			return sala;
		
			
			
			
		}
		}
		

	@Override
	public Sala findOne(Long id) {
		try {
			String sql = "SELECT id, oznaka , kapacitet, zakazan FROM sala WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new SalaRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public void update(Sala sala) {
		String sql="UPDATE sala SET  kapacitet=? "+
				" WHERE id = ? ";
				jdbcTemplate.update(sql,sala.getKapacitet(),sala.getId());
				
		
	}

	@Override
	public List<Sala> findAll() {
		try {
			String sql = "SELECT id, oznaka , kapacitet, zakazan FROM sala ";
			return jdbcTemplate.query(sql, new SalaRowMapper());
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public void save(Sala sala) {
		String sql = "INSERT INTO sala (  oznaka , kapacitet, zakazan) VALUES (?, ?, ?) ";
		 jdbcTemplate.update(sql, sala.getOznaka(), sala.getKapacitet(), sala.isZakazan());

		
	}

	@Override
	public void delete(Long id) {
		String sql = "DELETE FROM sala WHERE id = ? ";
		 jdbcTemplate.update(sql, id);
		
	}

	@Override
	public List<Sala> findSala(String oznaka) {
			ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT id, oznaka , kapacitet, zakazan FROM sala ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(oznaka!=null) {
			oznaka = "%" + oznaka + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("oznaka LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(oznaka);
		}
		
	
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY id";
		else
			sql=sql + " ORDER BY id";
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, listaArgumenata.toArray(), new SalaRowMapper());
		  
	}

}	


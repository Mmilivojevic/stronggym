package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.ClanskaKarta;

public interface ClanskaKartaDAO {
	
	public int save(ClanskaKarta clanskaKarta);
	public int update(ClanskaKarta clanskaKarta);
	public ClanskaKarta findOneByKorisnikId(Long korisnikId);
	public ClanskaKarta findOne(Long clanskaKartaId);
	public List<ClanskaKarta> findAll();

}

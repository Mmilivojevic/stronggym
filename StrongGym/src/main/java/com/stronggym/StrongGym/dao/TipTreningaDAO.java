package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.TipTreninga;

public interface TipTreningaDAO {
	public List<TipTreninga> findAll();
	public TipTreninga findOne(Long id);
	public List<TipTreninga> findByTreningId(Long id);
}

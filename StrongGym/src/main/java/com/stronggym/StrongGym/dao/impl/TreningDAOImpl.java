package com.stronggym.StrongGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.stronggym.StrongGym.dao.SlikaDAO;
import com.stronggym.StrongGym.dao.TipTreningaDAO;
import com.stronggym.StrongGym.dao.TreningDAO;
import com.stronggym.StrongGym.model.Slika;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.model.Trening;


@Repository
public class TreningDAOImpl implements TreningDAO {
	@Autowired
	SlikaDAO slikaDAO;
	@Autowired
	TipTreningaDAO tipTreningaDAO;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private class TreningSlikaTipRowCallBackHandler implements RowCallbackHandler {
		private Map<Long, Trening> treninzi = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			
			int index = 1;
			Long id = rs.getLong(index++);
			String naziv = rs.getString(index++);
			String treneri = rs.getString(index++);
			String kratakOpis = rs.getString(index++);
			Double cena = rs.getDouble(index++);
			String vrstaTreninga = rs.getString(index++);
			String nivoTreninga = rs.getString(index++);
			Double trajanjeTreninga = rs.getDouble(index++);
			
			Trening trening=treninzi.get(id);
			if(trening==null) {
				trening=new Trening(id, naziv,treneri, kratakOpis, cena, vrstaTreninga, nivoTreninga, trajanjeTreninga);
				treninzi.put(trening.getId(),trening);
			}
				
			Long slikaId=rs.getLong(index++);
			String imagePath=rs.getString(index++);
			Slika slika=new Slika(slikaId,imagePath);
			trening.getImagePath().add(slika);
			
			Long tipId = rs.getLong(index++);
			String tipNaziv = rs.getString(index++);
			String tipOpis = rs.getString(index++);
			TipTreninga tiTreninga = new TipTreninga(tipId, tipNaziv,tipOpis);
			trening.getTipTreninga().add(tiTreninga);

			
		}
		public List<Trening> getTreninzi(){
			return new ArrayList<Trening>(treninzi.values());
		}
	
	
	
	}
	private class TreningRowMapper implements RowMapper<Trening> {

		@Override
		public Trening mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String naziv = resultSet.getString(index++);
			String treneri = resultSet.getString(index++);
			String kratakOpis= resultSet.getString(index++);
			Double cena= resultSet.getDouble(index++);
			String vrstaTreninga= resultSet.getString(index++);
			String nivoTreninga= resultSet.getString(index++);
			Double trajanjeTreninga= resultSet.getDouble(index++);
			
			Trening trening= new Trening(id, naziv, treneri, kratakOpis, cena, vrstaTreninga, nivoTreninga, trajanjeTreninga);
			return trening;
			
			
		}
		
		
	}
	private class TreningTipTreningaRowMapper implements RowMapper<Long []> {

		@Override
		public Long [] mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long treningId = rs.getLong(index++);
			Long tipId = rs.getLong(index++);

			Long [] treningTipTreninga = {treningId, tipId};
			return treningTipTreninga;
		}
	}
private List<TipTreninga> findTreningTipTreninga(Long treningId, Long tipId) {
		
		List<TipTreninga> tipTreninga = new ArrayList<TipTreninga>();
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = 
				"SELECT tit.treningId, tit.tipId FROM treningtiptreninga tit ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(treningId!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("tit.treningId = ?");
			imaArgumenata = true;
			listaArgumenata.add(treningId);
		}
		
		if(tipId!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("tit.tipId = ?");
			imaArgumenata = true;
			listaArgumenata.add(tipId);
		}

		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY tit.treningId";
		else
			sql=sql + " ORDER BY tit.treningId";
		System.out.println(sql);
		
		List<Long[]> treningTipTreninga = jdbcTemplate.query(sql, listaArgumenata.toArray(), new TreningTipTreningaRowMapper()); 
				
		for (Long[] fz : treningTipTreninga) {
			tipTreninga.add(tipTreningaDAO.findOne(fz[1]));
		}
		return tipTreninga;
	}

private class TreningFotografijaRowMapper implements RowMapper<Long []> {

	@Override
	public Long [] mapRow(ResultSet rs, int rowNum) throws SQLException {
		int index = 1;
		Long treningId = rs.getLong(index++);
		Long slikaId = rs.getLong(index++);

		Long [] treningSlika = {treningId, slikaId};
		return treningSlika;
	}
}
private List<Slika> findTreningSlika(Long treningId, Long slikaId) {
	
	List<Slika> fotografija = new ArrayList<Slika>();
	
	ArrayList<Object> listaArgumenata = new ArrayList<Object>();
	
	String sql = 
			"SELECT treningId, slikaId FROM treningSlika ";
	
	StringBuffer whereSql = new StringBuffer(" WHERE ");
	boolean imaArgumenata = false;
	
	if(treningId!=null) {
		if(imaArgumenata)
			whereSql.append(" AND ");
		whereSql.append("treningId = ?");
		imaArgumenata = true;
		listaArgumenata.add(treningId);
	}
	
	if(slikaId!=null) {
		if(imaArgumenata)
			whereSql.append(" AND ");
		whereSql.append("slikaId = ?");
		imaArgumenata = true;
		listaArgumenata.add(slikaId);
	}

	if(imaArgumenata)
		sql=sql + whereSql.toString()+" ORDER BY treningId";
	else
		sql=sql + " ORDER BY treningId";
	System.out.println(sql);
	
	List<Long[]> treningFotografija = jdbcTemplate.query(sql, listaArgumenata.toArray(), new TreningFotografijaRowMapper()); 
			
	for (Long[] tf : treningFotografija) {
		fotografija.add(slikaDAO.findOne(tf[1]));
	}
	return fotografija;
}

	
	@Override
	public List<Trening> find(String naziv, Long tipId, Double cenaOd, Double cenaDo, String treneri,
			String vrstaTreninga, String nivoTreninga) {
		

		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT t.id, t.naziv, t.treneri, t.kratakOpis, t.cena,  t.vrstaTreninga ,  t.nivoTreninga , t.trajanjeTreninga  FROM trening t "; 
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(naziv!=null) {
			naziv = "%" + naziv + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.naziv LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(naziv);
		}
		if(treneri!=null) {
			treneri = "%" + treneri + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.treneri LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(treneri);
		}
		if(cenaOd!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.cena >= ?");
			imaArgumenata = true;
			listaArgumenata.add(cenaOd);
		}
		
		if(cenaDo!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.cena <= ?");
			imaArgumenata = true;
			listaArgumenata.add(cenaDo);
		}
		
		if(vrstaTreninga!=null) {
			vrstaTreninga = "%" + vrstaTreninga + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.vrstaTreninga LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(vrstaTreninga);
		}
		
		if(nivoTreninga!=null) {
			nivoTreninga = "%" + nivoTreninga + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.nivoTreninga LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(nivoTreninga);
		}
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY t.id";
		else
			sql=sql + " ORDER BY t.id";
		System.out.println(sql);
		
		List<Trening> treninzi = jdbcTemplate.query(sql, listaArgumenata.toArray(), new TreningRowMapper());
		for (Trening trening : treninzi) {
			
			trening.setTipTreninga(findTreningTipTreninga(trening.getId(), null));
		}
		
		
		for(Trening trening:treninzi) {
			trening.setImagePath(findTreningSlika(trening.getId(), null));
		}
		
				
		//ako se treži film sa određenim žanrom  
		// tada se taj žanr mora nalaziti u listi žanrova od filma
		if(tipId!=null)
			for (Iterator iterator = treninzi.iterator(); iterator.hasNext();) {
				Trening trening = (Trening) iterator.next();
				boolean zaBrisanje = true;
				for (TipTreninga tipTreninga : trening.getTipTreninga()) {
					if(tipTreninga.getId() == tipId) {
						zaBrisanje = false;
						break;
					}
				}
				if(zaBrisanje)
					iterator.remove();
			}
		
		return treninzi;
		

	}


	@Override
	public Trening findOne(Long id) {
		
		String sql = 
				"SELECT t.id, t.naziv, t.treneri, t.kratakOpis,  t.cena, t.vrstaTreninga, t.nivoTreninga, t.trajanjeTreninga ,  s.id, s.imagePath, tt.id, tt.naziv, tt.opis  FROM trening t " + 
				"LEFT JOIN treningSlika ts ON ts.treningId = t.id " + 
				"LEFT JOIN slika s ON ts.slikaId = s.id " + 
				"LEFT JOIN treningtiptreninga tit ON tit.treningId = t.id " + 
				"LEFT JOIN tiptreninga tt ON tit.tipId = tt.id " + 

				"WHERE t.id = ? " + 
				"ORDER BY t.id";

		TreningSlikaTipRowCallBackHandler rowCallbackHandler = new TreningSlikaTipRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getTreninzi().get(0);
	}

	@Transactional
	@Override
	public int save(Trening trening) {

			PreparedStatementCreator preparedStatementCreator=new PreparedStatementCreator() {
				
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					String sql="Insert into trening(naziv,treneri,kratakOpis,cena,vrstaTreninga,nivoTreninga,trajanjeTreninga) values(?,?,?,?,?,?,?)";
					
					PreparedStatement preparedStatement =connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
					int index=1;
					preparedStatement.setString(index++, trening.getNaziv());
					preparedStatement.setString(index++, trening.getTreneri());
					preparedStatement.setString(index++, trening.getKratakOpis());
					preparedStatement.setDouble(index++, trening.getCena());
					preparedStatement.setString(index++, trening.getVrstaTreninga());
					preparedStatement.setString(index++, trening.getNivoTreninga());
					preparedStatement.setDouble(index++, trening.getTrajanjeTreninga());
					return preparedStatement;
				}

		

				
			};
			GeneratedKeyHolder keyHolder=new GeneratedKeyHolder();
			boolean uspeh=jdbcTemplate.update(preparedStatementCreator,keyHolder)==1;
			
			if(uspeh) {
				String sql="Insert into treningslika(treningId,slikaId) values(?,? )";
				for(Slika itSlika:trening.getImagePath()) {
					uspeh=uspeh && jdbcTemplate.update(sql, keyHolder.getKey(),itSlika.getId())==1;
				}
			if(uspeh) {
				String sql2="Insert into treningtiptreninga(treningId,tipId) values(?,?)";
				for(TipTreninga itZanr:trening.getTipTreninga()) {
					uspeh=uspeh&&jdbcTemplate.update(sql2,keyHolder.getKey(),itZanr.getId())==1;
				}
			}
			}
			return uspeh?1:0;
		
	}

	@Transactional
	@Override
	public int update(Trening trening) {
		String sql="Delete from treningslika where treningId=?";
		 jdbcTemplate.update(sql,trening.getId());
		 
		 String sql1 = "DELETE FROM treningtiptreninga WHERE treningId = ?";
			jdbcTemplate.update(sql1, trening.getId());
		 
		boolean uspeh=true;
		sql1="insert into treningslika (treningId,slikaId) values(?,?)";
		for(Slika itSlika:trening.getImagePath()) {
			uspeh=uspeh && jdbcTemplate.update(sql1,trening.getId(),itSlika.getId())==1;
		}
		sql1="insert into treningtiptreninga (treningId, tipId) values(?,?)";
		for(TipTreninga itTipTreninga:trening.getTipTreninga()) {
			uspeh=uspeh && jdbcTemplate.update(sql1,trening.getId(),itTipTreninga.getId())==1;
		}

		sql1="UPDATE trening SET naziv=?, treneri=?, kratakOpis=?, cena=?, vrstaTreninga=?, nivoTreninga=?, trajanjeTreninga=?  where id=?";
		System.out.println("SQL za update:"+sql1);
		uspeh=uspeh && jdbcTemplate.update(sql1,trening.getNaziv(),trening.getTreneri(),trening.getKratakOpis(),trening.getCena(),trening.getVrstaTreninga(),trening.getNivoTreninga(),trening.getTrajanjeTreninga(),trening.getId())==1;
		return uspeh?1:0;

	}


	@Override
	public List<Trening> findAll() {
		try {
		String sql = 
				"SELECT t.id, t.naziv, t.treneri, t.kratakOpis,  t.cena, t.vrstaTreninga, t.nivoTreninga, t.trajanjeTreninga ,  s.id, s.imagePath, tt.id, tt.naziv, tt.opis  FROM trening t " + 
				"LEFT JOIN treningslika ts ON ts.treningId = t.id " + 
				"LEFT JOIN slika s ON ts.slikaId = s.id " + 
				"LEFT JOIN treningtiptreninga tit ON tit.treningId = t.id " + 
				"LEFT JOIN tiptreninga tt ON tit.tipId = tt.id " +  
				"ORDER BY t.id";

		TreningSlikaTipRowCallBackHandler rowCallbackHandler = new TreningSlikaTipRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getTreninzi();

		
	} catch (EmptyResultDataAccessException ex) {
		// ako korisnik nije pronađen
		return null;

	}
	}
}
	
	

	



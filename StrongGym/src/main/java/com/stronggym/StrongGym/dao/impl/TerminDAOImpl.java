package com.stronggym.StrongGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.stronggym.StrongGym.dao.TerminDAO;
import com.stronggym.StrongGym.model.Sala;
import com.stronggym.StrongGym.model.Slika;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.SalaService;

import com.stronggym.StrongGym.service.TreningService;

@Repository
public class TerminDAOImpl implements TerminDAO {
	@Autowired SalaService salaService;
	@Autowired TreningService treningService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class TerminRowMapper implements RowMapper<Termin> {
		private Map<Long, Termin> termini = new LinkedHashMap<>();
		
		
		@Override
		public Termin mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index=1;
			Long id=rs.getLong(index++);
			Long  treningId=rs.getLong(index++);
			Trening trening =treningService.findOne(treningId);
			Long  salaId=rs.getLong(index++);
			Sala sala =salaService.findOne(salaId);
			
			
			LocalDateTime datum=rs.getTimestamp(index++).toLocalDateTime();
		
			
			Termin termin=new Termin(id, trening, sala, datum);
			termini.put(termin.getId(), termin);
			return termin;
		
			
			
			
		}
		public List<Termin> getTermini(){
			return new ArrayList<Termin>(termini.values());
		}
	
	
		}

	@Override
	public Termin findOne(Long id) {
		try {
			String sql = "SELECT id, treningId ,salaId, datum FROM termini WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new TerminRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public List<Termin> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Termin termin) {
		
					String sql="Insert into termini (treningId , salaId, datum) values( ? , ?, ?)";
					jdbcTemplate.update(sql,termin.getTrening().getId(),termin.getSala().getId(),termin.getDatum());
		
	}
	
	@Override
	public void update(Termin termin) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Termin> findTermineZaTrening(Long treningId) {
		String sql="Select id, treningId ,salaId, datum from termini where treningId = ? order by id";
		return jdbcTemplate.query(sql,new TerminRowMapper(),treningId);

	}

	@Override
	public List<Termin> findTermineZaKorisnika(Long korisnikId) {
		String sql="Select t.id, t.treningId , t.salaId , datum from termini t " + 
				" LEFT JOIN  terminKorisnik tk on tk.terminId = t.id left join korisnik k on tk.korisnikId=k.id " + 
				" where k.id= ? ";
		System.out.println("SQL UPIT"+ sql);
		return jdbcTemplate.query(sql,new TerminRowMapper(),korisnikId);
		
	}
		
}

package com.stronggym.StrongGym.dao;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.stronggym.StrongGym.model.Korisnik;

public interface KorisnikDAO {
	public Korisnik findOne(Long id);
	public Korisnik findOne(String korisnickoIme);
	public List<Korisnik> findAll();
	public void update(Korisnik korisnik);
	public void update2(Korisnik korisnik);
	public List<Korisnik> findByKorisnickoIme(String korisnickoIme);
	public Korisnik findOne(String korisnickoIme, String lozinka);
	public void save(Korisnik korisnik);
	public void delete(Long id);
	public List<Korisnik> save(List<Korisnik> korisnici);
	public List<Korisnik> find(String korisnickoIme,Boolean administrator);
	public Korisnik findBlokiranogKorisnika(String korisnickoIme,String lozinka);
	public List<Korisnik> sviKorisniciZaTermin(Long terminId);
	 
	

	
}

package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TerminKorisnik;

public interface TerminKorisnikDAO {

	public int save(TerminKorisnik terminKorisnik);
	public int delete(Long terminId,Long korisnikId);
	public List<TerminKorisnik> findAllByKorisnikId(Long korisnikId);
	public List<TerminKorisnik> findAll();
}

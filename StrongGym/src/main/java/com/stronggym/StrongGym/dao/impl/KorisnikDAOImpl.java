package com.stronggym.StrongGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stronggym.StrongGym.dao.KorisnikDAO;
import com.stronggym.StrongGym.model.Korisnik;


@Repository
public class KorisnikDAOImpl implements KorisnikDAO {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KorisnikRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id=rs.getLong(index++);
			String korisnickoIme = rs.getString(index++);
			String lozinka = rs.getString(index++);
			String eMail = rs.getString(index++);
			String ime = rs.getString(index++);
			String prezime = rs.getString(index++);
			Date datumRodjenja =rs.getDate(index++);
			String adresa = rs.getString(index++);
			String brojTelefona = rs.getString(index++);
			Date datumRegistracije = rs.getDate(index++);
			Boolean administrator = rs.getBoolean(index++);
			Boolean block = rs.getBoolean(index++);

			Korisnik korisnik = new Korisnik(id,korisnickoIme, lozinka, eMail, ime, prezime,datumRodjenja,adresa,brojTelefona,datumRegistracije,administrator,block);
			return korisnik;
		}


}

	@Override
	public Korisnik findOne(Long id) {
		try {
			String sql = "SELECT id,korisnickoIme, lozinka ,email, ime, prezime , datumRodjenja, adresa, brojTelefona, datumRegistracije, administrator,block FROM korisnik WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public Korisnik findOne(String korisnickoIme) {
		try {
			String sql = "SELECT id,korisnickoIme, lozinka ,email, ime, prezime , datumRodjenja, adresa, brojTelefona, datumRegistracije, administrator,block FROM korisnik WHERE korisnickoIme = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public List<Korisnik>  findAll() {
		try {
			String sql = "SELECT id,korisnickoIme, lozinka ,email, ime, prezime , datumRodjenja, adresa, brojTelefona, datumRegistracije,block administrator from korisnik ";
			return jdbcTemplate.query(sql, new KorisnikRowMapper());
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public List<Korisnik> findByKorisnickoIme(String korisnickoIme) {
		try {
			String sql = "SELECT id,korisnickoIme, lozinka ,email, ime, prezime , datumRodjenja, adresa, brojTelefona, datumRegistracije, administrator FROM korisnik WHERE korisnickoIme = ?";
			return jdbcTemplate.query(sql, new KorisnikRowMapper(), korisnickoIme);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		try {
			String sql = "SELECT id, korisnickoIme, lozinka ,email, ime, prezime , datumRodjenja, adresa, brojTelefona, datumRegistracije, administrator,block FROM korisnik WHERE korisnickoIme = ? and lozinka = ? ";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme,lozinka);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public void save(Korisnik korisnik) {
		

			String sql = "INSERT INTO korisnik ( korisnickoIme, lozinka , email, ime, prezime , datumRodjenja, adresa, brojTelefona, datumRegistracije, administrator) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			 jdbcTemplate.update(sql, korisnik.getKorisnickoIme(), korisnik.getLozinka(), korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.getDatumRegistracije(), korisnik.isAdministrator());
		
	}
	
	@Override
	public List<Korisnik> save(List<Korisnik> korisnici) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Korisnik> find(String korisnickoIme, Boolean administrator) {
ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT id,korisnickoIme, lozinka ,email, ime, prezime , datumRodjenja, adresa, brojTelefona, datumRegistracije, administrator,block FROM korisnik ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(korisnickoIme!=null) {
			korisnickoIme = "%" + korisnickoIme + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("korisnickoIme LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(korisnickoIme);
		}
		
	
		
		
		if(administrator!=null) {	
			//vraća samo administratore ili sve korisnike sistema
			String administratorSql = (administrator)? "administrator = 1": "administrator >= 0";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(administratorSql);
			imaArgumenata = true;
		}
		
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY korisnickoIme";
		else
			sql=sql + " ORDER BY korisnickoIme";
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, listaArgumenata.toArray(), new KorisnikRowMapper());
		  
	}

	@Override
	public void update(Korisnik korisnik) {
		String sql="UPDATE korisnik SET administrator=? , block=? "+
				" WHERE korisnickoIme = ? ";
				jdbcTemplate.update(sql,korisnik.isAdministrator(),korisnik.isBlock(),korisnik.getKorisnickoIme());
				
	}

	@Override
	public Korisnik findBlokiranogKorisnika(String korisnickoIme, String lozinka) {
		try {
			String sql="SELECT id,korisnickoIme,lozinka,eMail,ime,prezime,datumRodjenja,adresa,brojTelefona,datumRegistracije,administrator,block "
					+ " FROM korisnik WHERE korisnickoIme=? AND lozinka=? ";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(),korisnickoIme,lozinka);
		}
		catch(EmptyResultDataAccessException ex){
			System.out.println("Korisnik nije pronadjen");
			return null;
		}
	}

	@Override
	public void update2(Korisnik korisnik) {
		String sql="UPDATE korisnik SET korisnickoIme=? , lozinka=?, eMail=?, ime=?, prezime=?, adresa=?, brojTelefona=?, datumRegistracije=?, datumRodjenja=? "+
				" WHERE korisnickoIme = ? ";
				jdbcTemplate.update(sql,korisnik.getKorisnickoIme(),korisnik.getLozinka(),korisnik.getEmail(),korisnik.getIme(),korisnik.getPrezime(),korisnik.getAdresa(),korisnik.getBrojTelefona(),korisnik.getDatumRegistracije(),korisnik.getDatumRodjenja(),korisnik.getKorisnickoIme());
				
		
	}

	@Override
	public List<Korisnik> sviKorisniciZaTermin(Long terminId) {
		String sql="SELECT k.id, korisnickoIme,lozinka,eMail,ime,prezime,datumRodjenja,adresa,brojTelefona,datumRegistracije,administrator,block "
				+ " FROM korisnik k "+
				"left join terminkorisnik tk on tk.korisnikId = k.id "+
				"left join termini t on  tk.terminId = t.id  where t.id = ? "+
				" order by k.id "
				;
				
		return jdbcTemplate.query(sql, new KorisnikRowMapper(),terminId);

	}


}
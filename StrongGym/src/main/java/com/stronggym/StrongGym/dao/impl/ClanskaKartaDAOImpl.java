package com.stronggym.StrongGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.ClanskaKartaDAO;
import com.stronggym.StrongGym.model.ClanskaKarta;
import com.stronggym.StrongGym.model.Komentar;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.KorisnikService;

@Repository
public class ClanskaKartaDAOImpl implements ClanskaKartaDAO {
	
	@Autowired KorisnikService korisnikService;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	private class ClanskaRowMapper implements RowMapper<ClanskaKarta> {

		@Override
		public ClanskaKarta mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index = 1;
			
			Long id = rs.getLong(index++);
			int brojPoena = rs.getInt(index++);
			String popust=rs.getString(index++);
			Long korisnikId = rs.getLong(index++);
			Korisnik korisnik = korisnikService.findOne(korisnikId);
			Boolean odobren=rs.getBoolean(index++);
			
			
			ClanskaKarta clanska =new ClanskaKarta(id,brojPoena,popust,korisnik,odobren );
				return clanska;
			
		}

	
}


	@Override
	public int save(ClanskaKarta clanskaKarta) {
		String sql = "INSERT INTO clanskaKartica (brojPoena , popust ,korisnikId , odobren ) VALUES (?, ?, ?, ?) ";
		return jdbcTemplate.update(sql, clanskaKarta.getBrojPoena(), clanskaKarta.getPopust(),clanskaKarta.getKorisnik().getId(),clanskaKarta.isOdobren());

	}


	@Override
	public int update(ClanskaKarta clanskaKarta) {
		String sql = "UPDATE clanskaKartica  SET brojPoena = ? , popust=? , odobren=? "
				+ " WHERE id  = ?";
		return jdbcTemplate.update(sql, clanskaKarta.getBrojPoena(),clanskaKarta.getPopust(),clanskaKarta.isOdobren(), clanskaKarta.getId());

	}


	@Override
	public ClanskaKarta findOneByKorisnikId(Long korisnikId) {
		String sql = 
				"SELECT ck.id, brojPoena, popust, korisnikId, odobren  FROM clanskaKartica ck  " + 
				"							LEFT JOIN korisnik k ON k.id = ck.korisnikId " + 
			
				"							WHERE  ck.korisnikId = ?  " + 
				"							 ORDER BY ck.id ";
		
		return jdbcTemplate.queryForObject(sql, new ClanskaRowMapper(), korisnikId);
	}


	@Override
	public ClanskaKarta findOne(Long clanskaKartaId) {
		String sql = 
				"SELECT ck.id, brojPoena, popust, korisnikId, odobren  FROM clanskaKartica ck  " 
				+" WHERE ck.id=? "+
				"							 ORDER BY ck.id ";
		
		return jdbcTemplate.queryForObject(sql, new ClanskaRowMapper(),clanskaKartaId);
	}


	@Override
	public List<ClanskaKarta> findAll() {
		String sql = 
				"SELECT ck.id, brojPoena, popust, korisnikId, odobren  FROM clanskaKartica ck  " + 
				
				"							 ORDER BY ck.id ";
		
		return jdbcTemplate.query(sql, new ClanskaRowMapper());
	}
}

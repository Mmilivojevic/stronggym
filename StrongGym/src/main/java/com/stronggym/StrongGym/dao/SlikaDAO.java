package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.Slika;



public interface SlikaDAO {
	public List<Slika> findAll();
	public Slika findOne(Long id);

}

package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.Komentar;

public interface KomentarDAO {
public Komentar findOne(Long id);
	
	public List<Komentar> findAll(Long treningId);
	
	public List<Komentar> findAll();
	
	public int save(Komentar komentar);
	
	public int update(Komentar komentar);
}

package com.stronggym.StrongGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stronggym.StrongGym.dao.SlikaDAO;
import com.stronggym.StrongGym.model.Slika;

@Repository
@Primary
public class SlikaDAOImpl implements SlikaDAO{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class SlikaRowMapper implements RowMapper<Slika> {

		@Override
		public Slika mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String imagePath= rs.getString(index++);
			
			Slika slika= new Slika(id, imagePath);
			return slika;
		}

	}

	@Override
	public List<Slika> findAll() {
		
			String sql="select id,imagePath from slika";
			
			return jdbcTemplate.query(sql,new SlikaRowMapper());
		

	}

	@Override
	public Slika findOne(Long id) {
		String sql = "SELECT id, imagePath FROM slika WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new SlikaRowMapper(), id);

	}

}

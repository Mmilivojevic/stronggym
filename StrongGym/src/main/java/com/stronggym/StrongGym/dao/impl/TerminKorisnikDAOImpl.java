package com.stronggym.StrongGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.TerminDAO;
import com.stronggym.StrongGym.dao.TerminKorisnikDAO;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Sala;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TerminKorisnik;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.KorisnikService;
import com.stronggym.StrongGym.service.TerminService;

@Service
public class TerminKorisnikDAOImpl implements TerminKorisnikDAO {
	@Autowired KorisnikService korisnikService;
	@Autowired TerminService terminService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private class TerminKorisnikRowMapper implements RowMapper<TerminKorisnik> {
	
		
		@Override
		public TerminKorisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index=1;
			
			Long terminId=rs.getLong(index++);
			Long  korisnikId=rs.getLong(index++);
			Termin termin =terminService.findOne(terminId);
			
			Korisnik korisnik =korisnikService.findOne(korisnikId);
			
			
		
			
			TerminKorisnik terminKorisnik=new TerminKorisnik(termin,korisnik);
			
			return terminKorisnik;
		
			
			
			
		}
	}


	@Override
	public int save(TerminKorisnik terminKorisnik) {
		String sql="Insert into terminkorisnik (terminId,korisnikId) values(?,?)";
		return jdbcTemplate.update(sql,terminKorisnik.getTermin().getId(),terminKorisnik.getKorisnik().getId());

	}
	@Override
	public int delete(Long terminId, Long korisnikId) {
		String sql = "DELETE FROM terminkorisnik WHERE terminId = ? and korisnikId=? ";
		 return jdbcTemplate.update(sql, terminId,korisnikId);
	}
	@Override
	public List<TerminKorisnik> findAllByKorisnikId(Long korisnikId) {
		String sql="Select tk.terminId, tk.korisnikId  from terminkorisnik tk " + 
				" LEFT JOIN  termini t on t.id = tk.terminId left join korisnik k on tk.korisnikId=k.id" + 
				" where k.id= ? ";
		System.out.println("SQL UPIT"+ sql);
		return jdbcTemplate.query(sql,new TerminKorisnikRowMapper(),korisnikId);
		
	}
	@Override
	public List<TerminKorisnik> findAll() {
		String sql="Select tk.terminId, tk.korisnikId  from terminkorisnik tk " ;
		System.out.println("SQL UPIT"+ sql);
		return jdbcTemplate.query(sql,new TerminKorisnikRowMapper());
	
	}
}
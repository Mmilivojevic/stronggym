package com.stronggym.StrongGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stronggym.StrongGym.dao.KomentarDAO;
import com.stronggym.StrongGym.dao.KorisnikDAO;
import com.stronggym.StrongGym.dao.TreningDAO;
import com.stronggym.StrongGym.model.Komentar;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Trening;

@Repository
public class KomentarDAOImpl implements KomentarDAO{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private TreningDAO treningDAO;
	@Autowired
	private KorisnikDAO korisnikDAO;

	private class KomentarRowMapper implements RowMapper<Komentar> {

		@Override
		public Komentar mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index = 1;
			
			Long komentarId = rs.getLong(index++);
			String tekst = rs.getString(index++);
			String ocena = rs.getString(index++);
		
			
			Date datumKomentara = rs.getTimestamp(index++);
			String status = rs.getString(index++);

			Long korisnikId = rs.getLong(index++);
			Korisnik autorKomentara = korisnikDAO.findOne(korisnikId);
			
			Long treningId = rs.getLong(index++);
			Trening komentarisanTrening = treningDAO.findOne(treningId);
			Boolean anoniman=rs.getBoolean(index++);
			
			
			Komentar komentar =new Komentar(komentarId, tekst,ocena, datumKomentara, status,autorKomentara, komentarisanTrening,anoniman );
				return komentar;
			
		}

	}
	@Override
	public Komentar findOne(Long id) {
		String sql = 
				"SELECT k.komentarId, k.tekst, k.ocena, k.datumKomentara, k.status, k.autorKomentara , k.komentarisanTrening, k.anoniman  FROM komentar k  " + 
				"							LEFT JOIN trening t ON t.id = k.komentarisanTrening " + 
				"    LEFT JOIN korisnik ko ON ko.id = k.autorKomentara "+
				"							WHERE  k.komentarId = ?  " + 
				"							 ORDER BY k.komentarId";
		
		return jdbcTemplate.queryForObject(sql, new KomentarRowMapper(), id);
		
	}

	@Override
	public List<Komentar> findAll(Long treningId) {

		String sql = 
				"SELECT k.komentarId, k.tekst, k.ocena, k.datumKomentara, k.status, k.autorKomentara,k.komentarisanTrening, k.anoniman  FROM komentar k " 
						+"LEFT JOIN trening t ON t.id = k.komentarisanTrening "
						+"LEFT JOIN korisnik ko ON ko.id = k.autorKomentara "
						+"WHERE  k.komentarisanTrening = ?  and status='odobren'  " 
						+" ORDER BY k.komentarId";
		return jdbcTemplate.query(sql, new KomentarRowMapper(), treningId);

	}
	@Override
	public List<Komentar> findAll() {
		String sql = 
				"SELECT k.komentarId, k.tekst, k.ocena, k.datumKomentara, k.status, k.autorKomentara, k.komentarisanTrening,k.anoniman  FROM komentar k " 
						+"LEFT JOIN trening t ON t.id = k.komentarisanTrening "
						+"LEFT JOIN korisnik ko ON ko.id = k.autorKomentara "
						+" ORDER BY k.komentarId";
		return jdbcTemplate.query(sql, new KomentarRowMapper());
	}

	@Override
	public int save(Komentar komentar) {
		String sql = "INSERT INTO komentar (tekst, ocena, datumKomentara, status, autorKomentara, komentarisanTrening,anoniman) VALUES (?, ?, ?, ?, ?, ?,?) ";
		return jdbcTemplate.update(sql, komentar.getTekstKomentara(), komentar.getOcena(), komentar.getDatumKomentara(), komentar.getStatus(),
																					komentar.getAutorKomentara().getId(),komentar.getKomentarisanTrening().getId(),komentar.isAnoniman());

	}

	@Override
	public int update(Komentar komentar) {
		String sql = "UPDATE komentar  SET status = ? "
				+ " WHERE komentarId  = ?";
		return jdbcTemplate.update(sql, komentar.getStatus(), komentar.getId());

	}
	
}

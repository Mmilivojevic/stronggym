package com.stronggym.StrongGym.dao;

import java.util.List;

import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Sala;

public interface SalaDAO {
	public Sala findOne(Long id);
	public void update(Sala Sala);
	public List<Sala> findAll();
	public void save(Sala sala);
	public void delete(Long id);
	public List<Sala> findSala(String oznaka);
}

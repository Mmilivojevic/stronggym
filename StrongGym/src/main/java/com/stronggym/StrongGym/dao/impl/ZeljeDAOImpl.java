package com.stronggym.StrongGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stronggym.StrongGym.dao.KorisnikDAO;
import com.stronggym.StrongGym.dao.TreningDAO;
import com.stronggym.StrongGym.dao.ZeljeDAO;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.model.Zelje;

@Repository
public class ZeljeDAOImpl implements ZeljeDAO{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private TreningDAO treningDAO;
	
	@Autowired
	private KorisnikDAO korisnikDAO;
	
	
	private class ZeljeRowMapper implements RowMapper<Zelje> {

		@Override
		public Zelje mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index = 1;
			
			Long id = rs.getLong(index++);
			
			Long treningId = rs.getLong(index++);
			
			Trening trening = treningDAO.findOne(treningId);
			
			Long korisnikId = rs.getLong(index++);
			
			Korisnik korisnik = korisnikDAO.findOne(korisnikId);
			
			
			Zelje zelje = new Zelje(id, korisnik, trening);
			return zelje;
			
		}

	}

	public List<Zelje> findAll(Long korisnikId) {
		String sql = 
				"SELECT z.id, t.id, ko.id FROM listaZelja z " 
				+"LEFT JOIN trening t ON t.id = z.treningId "
				+"LEFT JOIN korisnik ko ON ko.id = z.korisnikId "
				+" ORDER BY z.id";
		return jdbcTemplate.query(sql, new ZeljeRowMapper());
	}

	@Override
	public int save(Zelje zelje) {
		String sql = "INSERT INTO listaZelja (treningId, korisnikId) VALUES (?, ?) ";
		return jdbcTemplate.update(sql, zelje.getTrening().getId(), zelje.getKorisnik().getId());

	}

	@Override
	public int delete(Long zeljeId) {
		String sql = "DELETE FROM listaZelja WHERE treningId = ?";
		return jdbcTemplate.update(sql, zeljeId);
	}

	@Override
	public Zelje findOne(Long zeljeId) {
		String sql = 
				"SELECT z.id, z.treningId, z.korisnikId  FROM listaZelja z " 
						+"LEFT JOIN trening t ON t.id = z.treningId "
						+"LEFT JOIN korisnik ko  ON ko.id = z.korisnikId "
						+"WHERE z.id = ? " 
						+" ORDER BY z.id";
		
		return jdbcTemplate.queryForObject(sql, new ZeljeRowMapper(), zeljeId);
		
	}

	@Override
	public int delete(Long terminId, Long korisnikId) {
		String sql = "DELETE FROM listazelja WHERE treningId = ? and korisnikId=? ";
		 return jdbcTemplate.update(sql, terminId,korisnikId);
	
	


	}
	
}

package com.stronggym.StrongGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stronggym.StrongGym.dao.TipTreningaDAO;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.model.Trening;

@Repository
public class TipTreningaDAOImpl implements TipTreningaDAO {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private class TipTreningaRowMapper implements RowMapper<TipTreninga> {

		@Override
		public TipTreninga mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index = 1;
			Long id = rs.getLong(index++);
			String naziv = rs.getString(index++);
			String opis = rs.getString(index++);
			
			TipTreninga tipTreninga=new TipTreninga(id, naziv, opis);
			return tipTreninga;
		}

	}
	@Override
	public List<TipTreninga> findAll() {
		try {
			String sql = "SELECT id, naziv , opis FROM tipTreninga ";
			return jdbcTemplate.query(sql, new TipTreningaRowMapper());
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}
	@Override
	public TipTreninga findOne(Long id) {
		String sql = "SELECT id, naziv , opis FROM tiptreninga WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new TipTreningaRowMapper(), id);

	}
	@Override
	public List<TipTreninga> findByTreningId(Long id) {
		String sql="SELECT tt.id, tt.naziv , tt.opis FROM tiptreninga  tt" + 
				" LEFT JOIN  treningtiptreninga tit on tit.tipId = tt.id left join trening t on tit.treningId= t.id " + 
				" where t.id= ? ";
		System.out.println("SQL UPIT"+ sql);
		return jdbcTemplate.query(sql,new TipTreningaRowMapper(),id);
	}

}

package com.stronggym.StrongGym.service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.model.Korisnik;

public interface KorisnikService {
	Korisnik findOne(Long id);
	Korisnik findOne(String korisnickoIme);
	List<Korisnik> findAll();
	Korisnik update(Korisnik korisnik);
	Korisnik update2(Korisnik korisnik);

	List<Korisnik> findByKorisnickoIme(String korisnickoIme);
	Korisnik findOne(String korisnickoIme, String lozinka);
	Korisnik save(Korisnik korisnik);
	List<Korisnik> save(List<Korisnik> korisnici);
	 List<Korisnik> find(String korisnickoIme,Boolean administrator);
	 Korisnik findBlokiranogKorisnika(String korisnickoIme,String lozinka);
	 int korisniciZaTermin(Long terminId);
	 List<Korisnik> sviKorisniciZaTermin(Long terminId);
			  
	

}

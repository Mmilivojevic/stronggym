package com.stronggym.StrongGym.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.KorisnikDAO;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.service.KorisnikService;

@Service
public class DatabaseKorisnikService implements KorisnikService {
	@Autowired
	KorisnikDAO korisnikDAO;
	@Override
	public Korisnik findOne(Long id) {
		
		return korisnikDAO.findOne(id);
	}

	@Override
	public Korisnik findOne(String korisnickoIme) {
		
		return korisnikDAO.findOne(korisnickoIme);
	}

	@Override
	public List<Korisnik> findAll() {
		
		return korisnikDAO.findAll();
	}

	@Override
	public List<Korisnik> findByKorisnickoIme(String korisnickoIme) {
		
		return korisnikDAO.findByKorisnickoIme(korisnickoIme);
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		
		return korisnikDAO.findOne(korisnickoIme, lozinka);
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		
		 korisnikDAO.save(korisnik);
		 System.out.println("Korisnik reg  "+korisnik);
		return korisnik;
		
	}

	@Override
	public List<Korisnik> save(List<Korisnik> korisnici) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Korisnik> find(String korisnickoIme, Boolean administrator) {
		// TODO Auto-generated method stub
		return korisnikDAO.find(korisnickoIme, administrator);
	}

	@Override
	public Korisnik update(Korisnik korisnik) {
		korisnikDAO.update(korisnik);
		return korisnik;
	}

	@Override
	public Korisnik findBlokiranogKorisnika(String korisnickoIme, String lozinka) {
		return korisnikDAO.findBlokiranogKorisnika(korisnickoIme, lozinka);
		
	}

	@Override
	public Korisnik update2(Korisnik korisnik) {
		korisnikDAO.update2(korisnik);
		return korisnik;
	}

	@Override
	public List<Korisnik> sviKorisniciZaTermin(Long terminId) {
		return korisnikDAO.sviKorisniciZaTermin(terminId);
	
	}

	@Override
	public int korisniciZaTermin(Long terminId) {
		List<Korisnik> korisniciZaTermin=new ArrayList<Korisnik>();
		for(Korisnik korisnik:sviKorisniciZaTermin(terminId)) {
			korisniciZaTermin.add(korisnik);
		}
		int brojac=korisniciZaTermin.size();
		return brojac;
	}


}

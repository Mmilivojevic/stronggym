package com.stronggym.StrongGym.service;

import java.util.List;

import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TerminKorisnik;

public interface TerminKorisnikService {
	TerminKorisnik save(TerminKorisnik terminKorisnik);
	int delete(Long terminId,Long korisnikId);
	List<TerminKorisnik> findAllByKorisnikId(Long korisnikId);
	List<TerminKorisnik> findAll();
}

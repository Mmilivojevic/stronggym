package com.stronggym.StrongGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.ClanskaKartaDAO;
import com.stronggym.StrongGym.model.ClanskaKarta;
import com.stronggym.StrongGym.service.ClanskaKartaService;

@Service
public class DatabaseClanskaKartaService implements ClanskaKartaService{
	@Autowired ClanskaKartaDAO clanskaDAO;
	
	@Override
	public ClanskaKarta save(ClanskaKarta clanskaKarta) {
		clanskaDAO.save(clanskaKarta);
		return clanskaKarta;
	}

	@Override
	public int update(ClanskaKarta clanskaKarta) {
		return  clanskaDAO.update(clanskaKarta);
		
	}

	@Override
	public ClanskaKarta findOneByKorisnikId(Long korisnikId) {
		
		return clanskaDAO.findOneByKorisnikId(korisnikId);
	}

	@Override
	public ClanskaKarta findOne(Long clanskaKartaId) {
		
		return clanskaDAO.findOne(clanskaKartaId);
	}

	@Override
	public List<ClanskaKarta> findAll() {
		return clanskaDAO.findAll();
	}
	
	

}

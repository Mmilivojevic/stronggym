package com.stronggym.StrongGym.service;

import java.util.List;

import com.stronggym.StrongGym.model.Komentar;

public interface KomentarService {
	Komentar findOne(Long id);
	
	List<Komentar> findAll(Long treningId);
	
	List<Komentar> findAll();
	
	Komentar save(Komentar komentar);
	
	Komentar update(Komentar komentar);

}

package com.stronggym.StrongGym.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.TerminDAO;
import com.stronggym.StrongGym.model.Sala;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.KorisnikService;
import com.stronggym.StrongGym.service.SalaService;
import com.stronggym.StrongGym.service.TerminService;
import com.stronggym.StrongGym.service.TreningService;

@Service
public class DatabaseTerminService implements TerminService {
	@Autowired TerminDAO terminDAO;
	@Autowired TreningService treningService;
	@Autowired KorisnikService korisnikService;
	@Autowired SalaService salaService;
	@Override
	public Termin findOne(Long id) {
		return terminDAO.findOne(id);
	
	}

	@Override
	public List<Termin> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Termin termin) {
		terminDAO.save(termin);
	}

	@Override
	public void update(Termin termin) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Termin> findTermineZaTrening(Long treningId) {
		return terminDAO.findTermineZaTrening(treningId);
		
	}

	@Override
	public List<Termin> findTermineZaKorisnika(Long korisnikId) {
		
		return terminDAO.findTermineZaKorisnika(korisnikId);
	}

	@Override
	public List<Termin> findNepopunjeTermineZaJedanTrening(Long treningId) {
		List<Termin> findTermin=new ArrayList<Termin>();
		Trening trening=treningService.findOne(treningId);
		
			for(Termin termin:findTermineZaTrening(trening.getId())) {
				int brojac=korisnikService.korisniciZaTermin(termin.getId());
				System.out.println("Brojac korisnika"+brojac);
				Sala sala=salaService.findOne(termin.getSala().getId());
				if(sala.getKapacitet() == brojac) {
					break;
				}
				
				if(sala.getKapacitet() > brojac) {
					
					findTermin.add(termin);
				}
			}
		return findTermin;
	}

	@Override
	public Boolean zakazanTermin(Long korisnikId, Long treningId) {
		List<Termin> termini=findTermineZaKorisnika(korisnikId);
		for(Termin termin:termini) {
			Long idTreninga=termin.getTrening().getId();
			if(treningId.equals(idTreninga)) {
				return true;
			}
		}
		return false;
	}

}

package com.stronggym.StrongGym.service;

import java.util.List;

import com.stronggym.StrongGym.model.ClanskaKarta;

public interface ClanskaKartaService {
	
	 ClanskaKarta save(ClanskaKarta clanskaKarta);
	 int update(ClanskaKarta clanskaKarta);
	 ClanskaKarta findOneByKorisnikId(Long korisnikId);
	 ClanskaKarta findOne(Long clanskaKartaId);
	 List<ClanskaKarta> findAll();

}

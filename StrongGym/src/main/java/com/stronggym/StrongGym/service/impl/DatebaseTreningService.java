package com.stronggym.StrongGym.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.TreningDAO;
import com.stronggym.StrongGym.model.Sala;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.KorisnikService;
import com.stronggym.StrongGym.service.SalaService;
import com.stronggym.StrongGym.service.TerminService;
import com.stronggym.StrongGym.service.TreningService;

@Service
public class DatebaseTreningService implements TreningService {
	@Autowired
	TreningDAO treningDAO;
	@Autowired SalaService salaService;
	@Autowired TerminService terminService;
	@Autowired KorisnikService korisnikService;
	
//	@Override
//	public List<Trening> find(String naziv, String treneri, String kratakOpis, String slika, double cena,
//			String vrstaTreninga, String nivoTreninga, String trajanjeTreninga) {
//		
//		return treningDAO.find(naziv, treneri, kratakOpis, slika, cena, vrstaTreninga, nivoTreninga, trajanjeTreninga);
//	}
//	@Override

	@Override
	public List<Trening> find(String naziv, Long tipId, Double cenaOd, Double cenaDo, String treneri,
			String vrstaTreninga, String nivoTreninga) {
		
		return treningDAO.find(naziv, tipId, cenaOd, cenaDo, treneri, vrstaTreninga, nivoTreninga);
	}

	@Override
	public Trening findOne(Long id) {
		
		return treningDAO.findOne(id);
	}

	@Override
	public Trening save(Trening trening) {
		treningDAO.save(trening);
		return trening;
	}

	@Override
	public Trening update(Trening trening) {
		treningDAO.update(trening);
		return trening;
	}

	@Override
	public List<Trening> findAll() {
		return treningDAO.findAll();
	
	}

	@Override
	public List<Trening> nadjiNepopunjeneTreninge() {
		
		Set<Trening> dodaj=new HashSet<>() ;
		for(Trening trening:findAll()) {
			for(Termin termin:terminService.findTermineZaTrening(trening.getId())) {
				int brojac=korisnikService.korisniciZaTermin(termin.getId());
				Sala sala=salaService.findOne(termin.getSala().getId());
				if(sala.getKapacitet() == brojac) {
					break;
				}
				if(sala.getKapacitet() > brojac) {
					
					dodaj.add(trening);
				}
			}
		}
		List<Trening> findNepopunjeniTreninzi=new ArrayList<>(dodaj);
		return findNepopunjeniTreninzi;
	}


}

package com.stronggym.StrongGym.service;

import java.util.List;

import com.stronggym.StrongGym.model.Zelje;

public interface ZeljeService {
	
	List<Zelje> findAll(Long korisnikId);
	int save(Zelje zelje);
	
	Zelje findOne(Long zeljeId);
	 int delete(Long terminId,Long korisnikId);
	 Zelje delete(Long zelje);
		

}

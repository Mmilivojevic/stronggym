package com.stronggym.StrongGym.service;

import java.util.List;

import com.stronggym.StrongGym.model.Slika;

public interface SlikaService {
	Slika findOne(Long id);
	
	List<Slika> findAll();
	List<Slika> find(Long[] ids);
	Slika save(Slika slika);
	List<Slika> save(List<Slika> slika);
	Slika update(Slika Slika);
	List<Slika> update(List<Slika>slike);
	Slika delete(Long id);
	void delete(List<Long> ids);
	List<Slika> find(String naziv);
}

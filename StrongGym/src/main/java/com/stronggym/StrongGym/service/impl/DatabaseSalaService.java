package com.stronggym.StrongGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.SalaDAO;
import com.stronggym.StrongGym.model.Sala;
import com.stronggym.StrongGym.service.SalaService;

@Service
public class DatabaseSalaService implements SalaService{
	
	@Autowired
	SalaDAO salaDAO;

	@Override
	public Sala findOne(Long id) {
		
		return salaDAO.findOne(id);
	}

	@Override
	public void update(Sala Sala) {
		salaDAO.update(Sala);
		
	}

	@Override
	public List<Sala> findAll() {
	
		return salaDAO.findAll();
	}

	@Override
	public void save(Sala sala) {
		salaDAO.save(sala);
	}

	@Override
	public void delete(Long id) {
		salaDAO.delete(id);
		
	}

	@Override
	public List<Sala> findSala(String oznaka) {
		
		return salaDAO.findSala(oznaka);
	}
	
	

}

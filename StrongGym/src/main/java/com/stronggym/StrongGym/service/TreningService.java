package com.stronggym.StrongGym.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.model.Trening;

public interface TreningService {
	List<Trening> find(String naziv, Long tipId, Double cenaOd, Double cenaDo, String treneri, String vrstaTreninga, String nivoTreninga);
	
	Trening findOne(Long id );
	Trening save(Trening trening);
	Trening update(Trening trening);
	List<Trening> findAll();
	List<Trening> nadjiNepopunjeneTreninge();
}

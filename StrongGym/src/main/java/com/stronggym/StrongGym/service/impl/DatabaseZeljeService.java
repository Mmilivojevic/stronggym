package com.stronggym.StrongGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.ZeljeDAO;
import com.stronggym.StrongGym.model.Zelje;
import com.stronggym.StrongGym.service.ZeljeService;

@Service
public class DatabaseZeljeService implements ZeljeService{
	
	@Autowired ZeljeDAO zeljeDAO;

	@Override
	public List<Zelje> findAll(Long korisnikId) {
		
		return zeljeDAO.findAll(korisnikId);
	}

	@Override
	public int save(Zelje zelje) {
		zeljeDAO.save(zelje);
		return 0;
	}

	@Override
	public Zelje delete(Long zeljeId) {
		Zelje zelje=findOne(zeljeId);
		if(zeljeId != null) {
			zeljeDAO.delete(zeljeId);
		}
		return zelje;
	}

	@Override
	public Zelje findOne(Long zeljeId) {
		
		return zeljeDAO.findOne(zeljeId);
	}

	@Override
	public int delete(Long terminId, Long korisnikId) {
		return zeljeDAO.delete(terminId,korisnikId);
	}

}

package com.stronggym.StrongGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stronggym.StrongGym.dao.TerminKorisnikDAO;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TerminKorisnik;
import com.stronggym.StrongGym.service.TerminKorisnikService;

@Repository
public class DatabaseTerminKorisnikService implements TerminKorisnikService{
	@Autowired TerminKorisnikDAO terminKorisnikDAO;
	@Override
	public TerminKorisnik save(TerminKorisnik terminKorisnik) {
		terminKorisnikDAO.save(terminKorisnik);
		return terminKorisnik ;
	}

	@Override
	public int delete(Long terminId, Long korisnikId) {
		return terminKorisnikDAO.delete(terminId, korisnikId);
	}

	@Override
	public List<TerminKorisnik> findAllByKorisnikId(Long korisnikId) {
		
		return terminKorisnikDAO.findAllByKorisnikId(korisnikId);
	}

	@Override
	public List<TerminKorisnik> findAll() {
		
		return terminKorisnikDAO.findAll();
	}

}

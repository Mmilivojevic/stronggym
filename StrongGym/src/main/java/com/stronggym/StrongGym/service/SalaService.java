package com.stronggym.StrongGym.service;

import java.util.List;

import com.stronggym.StrongGym.model.Sala;

public interface SalaService {
	Sala findOne(Long id);
	void update(Sala Sala);
	 List<Sala> findAll();
	 void save(Sala sala);
	 void delete(Long id);
	 List<Sala> findSala(String oznaka);

}

package com.stronggym.StrongGym.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.model.TipTreninga;


public interface TipTreningaService {
	List<TipTreninga> findAll();
	List<TipTreninga> find(Long[] ids);
	 List<TipTreninga> findByTreningId(Long id);
}

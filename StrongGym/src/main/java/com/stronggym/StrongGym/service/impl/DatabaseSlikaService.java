package com.stronggym.StrongGym.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.SlikaDAO;
import com.stronggym.StrongGym.model.Slika;
import com.stronggym.StrongGym.service.SlikaService;


@Service
public class DatabaseSlikaService implements SlikaService {

	@Autowired
	SlikaDAO slikaDAO;

	@Override
	public Slika findOne(Long id) {
		
		return slikaDAO.findOne(id);
	}

	@Override
	public List<Slika> findAll() {
		
		return slikaDAO.findAll();
	}

	@Override
	public List<Slika> find(Long[] ids) {
		List<Slika> rezultat = new ArrayList<>();
		for (Long id: ids) {
			Slika slika = slikaDAO.findOne(id);
			rezultat.add(slika);
		}

		return rezultat;
	}

	@Override
	public Slika save(Slika slika) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Slika> save(List<Slika> slika) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Slika update(Slika Slika) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Slika> update(List<Slika> slike) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Slika delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(List<Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Slika> find(String naziv) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

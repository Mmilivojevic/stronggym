package com.stronggym.StrongGym.service;

import java.util.List;

import com.stronggym.StrongGym.model.Termin;

public interface TerminService {
	
	Termin findOne(Long id);
	List<Termin> findAll();
	void save(Termin termin);
	void update(Termin termin);
	void delete(Long id);
	List<Termin> findTermineZaTrening( Long treningId);
	List<Termin> findTermineZaKorisnika(Long korisnikId);
	List<Termin> findNepopunjeTermineZaJedanTrening(Long treningId);
	Boolean zakazanTermin(Long korisnikId,Long treningId);

}

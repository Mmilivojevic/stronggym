package com.stronggym.StrongGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.KomentarDAO;
import com.stronggym.StrongGym.model.Komentar;
import com.stronggym.StrongGym.service.KomentarService;

@Service
public class DatabaseKomentarService implements KomentarService{
	
	@Autowired
	KomentarDAO komentarDAO;

	@Override
	public Komentar findOne(Long id) {
		Komentar komentar=komentarDAO.findOne(id);
		return komentar;
	}

	@Override
	public List<Komentar> findAll(Long treningId) {
		return komentarDAO.findAll(treningId);
	}

	@Override
	public List<Komentar> findAll() {
		return komentarDAO.findAll();
	}

	@Override
	public Komentar save(Komentar komentar) {
		 komentarDAO.save(komentar);
		return komentar;
		
		
	}

	@Override
	public Komentar update(Komentar komentar) {
		komentarDAO.update(komentar);
		return komentar;
	}

}

package com.stronggym.StrongGym.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stronggym.StrongGym.dao.TipTreningaDAO;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.service.TipTreningaService;

@Service
public class DatabaseTipTreningaService implements TipTreningaService {
	@Autowired
	TipTreningaDAO tipTreningaDAO;

	@Override
	public List<TipTreninga> findAll() {
		
		return tipTreningaDAO.findAll();
	}

	@Override
	public List<TipTreninga> find(Long[] ids) {
		List<TipTreninga> rezultat = new ArrayList<>();
		for (Long id: ids) {
			TipTreninga tipTreninga = tipTreningaDAO.findOne(id);
			rezultat.add(tipTreninga);
		}

		return rezultat;
	}

	@Override
	public List<TipTreninga> findByTreningId(Long id) {
		
		return tipTreningaDAO.findByTreningId(id);
	}
	
}

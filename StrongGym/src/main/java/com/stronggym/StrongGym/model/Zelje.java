package com.stronggym.StrongGym.model;

public class Zelje {
	private Long id;
	private Korisnik korisnik;
	private Trening trening;
	public Zelje() {
		super();
	}
	
	public Zelje(Korisnik korisnik, Trening trening) {
		super();
		this.korisnik = korisnik;
		this.trening = trening;
	}

	public Zelje(Long id, Korisnik korisnik, Trening trening) {
		super();
		this.id = id;
		this.korisnik = korisnik;
		this.trening = trening;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public Trening getTrening() {
		return trening;
	}
	public void setTrening(Trening trening) {
		this.trening = trening;
	}
	@Override
	public String toString() {
		return "Zelje [id=" + id + ", korisnik=" + korisnik + ", trening=" + trening + "]";
	}
	
	


}

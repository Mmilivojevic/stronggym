package com.stronggym.StrongGym.model;

public class TerminKorisnik {
	
	public Korisnik korisnik;
	public Termin termin;
	public TerminKorisnik( Termin termin,Korisnik korisnik) {
		super();
		
		this.termin = termin;
		this.korisnik = korisnik;
	}
	public TerminKorisnik() {
		super();
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public Termin getTermin() {
		return termin;
	}
	public void setTermin(Termin termin) {
		this.termin = termin;
	}
	@Override
	public String toString() {
		return "TerminKorisnik [korisnik=" + korisnik + ", termin=" + termin + "]";
	}
	
	

}

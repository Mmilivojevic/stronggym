package com.stronggym.StrongGym.model;


import java.util.ArrayList;
import java.util.List;


public class Trening {
	private Long id;
	private String naziv="";
	private String treneri="";
	private String kratakOpis="";
	private List<Slika> imagePath= new ArrayList<>();
	
	private List<TipTreninga> tipTreninga=new ArrayList();
	private double cena=0;
	//default u bazi
	private String vrstaTreninga="pojedinacni";
	//default u bazi
	private String nivoTreninga="amaterski";
	private double trajanjeTreninga=0;
	
	public Trening() {
		super();
	}
	
	public Trening( String naziv, String treneri, String kratakOpis, double cena, String vrstaTreninga,
			String nivoTreninga, double trajanjeTreninga) {
		super();
		
		this.naziv = naziv;
		this.treneri = treneri;
		this.kratakOpis = kratakOpis;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
	}

	public Trening(String naziv, String treneri, String kratakOpis, List<Slika> imagePath,
			List<TipTreninga> tipTreninga, double cena, String vrstaTreninga, String nivoTreninga,
			double trajanjeTreninga) {
		super();
		this.naziv = naziv;
		this.treneri = treneri;
		this.kratakOpis = kratakOpis;
		this.imagePath = imagePath;
		this.tipTreninga = tipTreninga;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
	}

	public Trening(Long id, String naziv, String treneri, String kratakOpis, List<Slika> imagePath,
			List<TipTreninga> tipTreninga, double cena, String vrstaTreninga, String nivoTreninga,
			double trajanjeTreninga) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.treneri = treneri;
		this.kratakOpis = kratakOpis;
		this.imagePath = imagePath;
		this.tipTreninga = tipTreninga;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
	}


	



	public Trening(Long id, String naziv, String treneri, String kratakOpis, double cena, String vrstaTreninga,
			String nivoTreninga, double trajanjeTreninga) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.treneri = treneri;
		this.kratakOpis = kratakOpis;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
	}

	@Override
	public String toString() {
		return "Trening [id=" + id + ", naziv=" + naziv + ", treneri=" + treneri + ", kratakOpis=" + kratakOpis
				+ ", imagePath=" + imagePath + ", tipTreninga=" + tipTreninga + ", cena=" + cena + ", vrstaTreninga="
				+ vrstaTreninga + ", nivoTreninga=" + nivoTreninga + ", trajanjeTreninga=" + trajanjeTreninga + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime*result + ((id == null) ? 0 : id.hashCode());
		return 31 + id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trening other = (Trening) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getTreneri() {
		return treneri;
	}

	public void setTreneri(String treneri) {
		this.treneri = treneri;
	}

	public String getKratakOpis() {
		return kratakOpis;
	}

	public void setKratakOpis(String kratakOpis) {
		this.kratakOpis = kratakOpis;
	}

	public List<Slika> getImagePath() {
		
		return imagePath;
	}

	public void setImagePath(List<Slika> imagePath) {
		this.imagePath=imagePath;
	}

	public List<TipTreninga> getTipTreninga() {
		return tipTreninga;
	}

	public void setTipTreninga(List<TipTreninga> tipTreninga) {
		this.tipTreninga = tipTreninga;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public String getVrstaTreninga() {
		return vrstaTreninga;
	}

	public void setVrstaTreninga(String vrstaTreninga) {
		this.vrstaTreninga = vrstaTreninga;
	}

	public String getNivoTreninga() {
		return nivoTreninga;
	}

	public void setNivoTreninga(String nivoTreninga) {
		this.nivoTreninga = nivoTreninga;
	}

	public double getTrajanjeTreninga() {
		return trajanjeTreninga;
	}

	public void setTrajanjeTreninga(double trajanjeTreninga) {
		this.trajanjeTreninga = trajanjeTreninga;
	}


	


}

package com.stronggym.StrongGym.model;

public class ClanskaKarta {
	
	public Long id;
	public int brojPoena;
	public Korisnik korisnik;
	public String popust;
	public boolean odobren=false;
	public ClanskaKarta() {
		super();
	}
	public ClanskaKarta(int brojPoena, Korisnik korisnik, String popust, boolean odobren) {
		super();
		this.brojPoena = brojPoena;
		this.korisnik = korisnik;
		this.popust = popust;
		this.odobren = odobren;
	}
	public ClanskaKarta(Long id, int brojPoena, String popust, Korisnik korisnik, boolean odobren) {
		super();
		this.id = id;
		this.brojPoena = brojPoena;
		
		this.popust = popust;
		this.korisnik = korisnik;
		this.odobren = odobren;
	}
	public ClanskaKarta(int brojPoena, Korisnik korisnik, String popust) {
		super();
		this.brojPoena = brojPoena;
		this.korisnik = korisnik;
		this.popust = popust;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getBrojPoena() {
		return brojPoena;
	}
	public void setBrojPoena(int brojPoena) {
		this.brojPoena = brojPoena;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public String getPopust() {
		return popust;
	}
	public void setPopust(String popust) {
		this.popust = popust;
	}
	public boolean isOdobren() {
		return odobren;
	}
	public void setOdobren(boolean odobren) {
		this.odobren = odobren;
	}
	@Override
	public String toString() {
		return "ClanskaKarta [id=" + id + ", brojPoena=" + brojPoena + ", korisnik=" + korisnik + ", popust=" + popust
				+ ", odobren=" + odobren + "]";
	}
	
	
	

}

package com.stronggym.StrongGym.model;

public class Sala {
	public Long id;
	public String oznaka;
	public int kapacitet;
	public boolean zakazan=false;
	public Sala() {
		super();
	}
	
	public Sala(String oznaka, int kapacitet) {
		super();
		this.oznaka = oznaka;
		this.kapacitet = kapacitet;
	}

	public Sala(Long id, String oznaka, int kapacitet, boolean zakazan) {
		super();
		this.id = id;
		this.oznaka = oznaka;
		this.kapacitet = kapacitet;
		this.zakazan = zakazan;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOznaka() {
		return oznaka;
	}
	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}
	public boolean isZakazan() {
		return zakazan;
	}
	public void setZakazan(boolean zakazan) {
		this.zakazan = zakazan;
	}
	@Override
	public String toString() {
		return "Sala [id=" + id + ", oznaka=" + oznaka + ", kapacitet=" + kapacitet + ", zakazan=" + zakazan + "]";
	}
	
	
	
	
	
}

package com.stronggym.StrongGym.model;

import java.time.LocalDateTime;
import java.util.Date;

public class Termin {

	public Long id;
	public Sala sala;
	public Trening trening;
	public LocalDateTime datum;
	public Termin() {
		super();
	}
	
	public Termin(Trening trening, Sala sala, LocalDateTime datum) {
		super();
		
		this.trening = trening;
		this.sala = sala;
		this.datum = datum;
	}

	
	public Termin(Long id, Sala sala, Trening trening, LocalDateTime datum) {
		super();
		this.id = id;
		this.sala = sala;
		this.trening = trening;
		this.datum = datum;
	}

	public Termin(Long id, Trening trening, Sala sala, LocalDateTime datum) {
		super();
		this.id = id;
		this.trening = trening;
		this.sala = sala;
		
		this.datum = datum;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Sala getSala() {
		return sala;
	}
	public void setSala(Sala sala) {
		this.sala = sala;
	}
	public Trening getTrening() {
		return trening;
	}
	public void setTrening(Trening trening) {
		this.trening = trening;
	}
	public LocalDateTime getDatum() {
		return datum;
	}
	public void setDatum(LocalDateTime datum) {
		this.datum = datum;
	}
	@Override
	public String toString() {
		return "Termin [id=" + id + ", sala=" + sala + ", trening=" + trening + ", datum=" + datum + "]";
	}
	
	
}

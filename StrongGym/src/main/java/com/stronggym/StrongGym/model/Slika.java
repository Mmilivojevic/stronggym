package com.stronggym.StrongGym.model;

public class Slika {
	private Long id;
	private String imagePath;
	
	public Slika() {
		super();
	}

	public Slika(Long id, String imagePath) {
		super();
		this.id = id;
		this.imagePath = imagePath;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Override
	public String toString() {
		return "Slika [id=" + id + ",imagePath=" + imagePath + "]";
	}
	
	
	

}

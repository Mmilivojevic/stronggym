package com.stronggym.StrongGym.model;

import java.util.Date;

public class Komentar {
	private Long id;
	private String tekstKomentara;
	private String ocena;
	private Date datumKomentara;
	private String status="na cekanju";
	private Korisnik autorKomentara;
	private Trening komentarisanTrening;
	
	public boolean anoniman;
	
	public Komentar() {
		super();
	}

	
	

	public Komentar(Long id, String tekstKomentara, String ocena, Date datumKomentara, String status,
			Korisnik autorKomentara, Trening komentarisanTrening,boolean anoniman) {
		super();
		this.id = id;
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datumKomentara = datumKomentara;
		this.status = status;
		this.autorKomentara = autorKomentara;
		this.komentarisanTrening = komentarisanTrening;
		this.anoniman=anoniman;
	}
	



	public Komentar(String tekstKomentara, String ocena, Date datumKomentara, String status, Korisnik autorKomentara,
			Trening komentarisanTrening, boolean anoniman) {
		super();
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datumKomentara = datumKomentara;
		this.status = status;
		this.autorKomentara = autorKomentara;
		this.komentarisanTrening = komentarisanTrening;
		this.anoniman = anoniman;
	}




	public Komentar(String tekstKomentara, String ocena, Date datumKomentara, Korisnik autorKomentara,
			Trening komentarisanTrening, String status) {
		super();
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datumKomentara = datumKomentara;
		this.autorKomentara = autorKomentara;
		this.komentarisanTrening = komentarisanTrening;
		this.status = status;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTekstKomentara() {
		return tekstKomentara;
	}

	public void setTekstKomentara(String tekstKomentara) {
		this.tekstKomentara = tekstKomentara;
	}

	public String getOcena() {
		return ocena;
	}

	public void setOcena(String ocena) {
		this.ocena = ocena;
	}

	public Date getDatumKomentara() {
		return datumKomentara;
	}

	public void setDatumKomentara(Date datumKomentara) {
		this.datumKomentara = datumKomentara;
	}

	public Korisnik getAutorKomentara() {
		return autorKomentara;
	}

	public void setAutorKomentara(Korisnik autorKomentara) {
		this.autorKomentara = autorKomentara;
	}



	public Trening getKomentarisanTrening() {
		return komentarisanTrening;
	}




	public void setKomentarisanTrening(Trening komentarisanTrening) {
		this.komentarisanTrening = komentarisanTrening;
	}




	public boolean isAnoniman() {
		return anoniman;
	}




	@Override
	public String toString() {
		return "Komentar [id=" + id + ", tekstKomentara=" + tekstKomentara + ", ocena=" + ocena + ", datumKomentara="
				+ datumKomentara + ", status=" + status + ", autorKomentara=" + autorKomentara
				+ ", komentarisanTrening=" + komentarisanTrening + ", anoniman=" + anoniman + "]";
	}




	public void setAnoniman(boolean anoniman) {
		this.anoniman = anoniman;
	}




	public String getStatus() {
		return status;
	}




	public void setStatus(String status) {
		this.status = status;
	}




	


}

package com.stronggym.StrongGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.stronggym.StrongGym.model.ClanskaKarta;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.service.ClanskaKartaService;
import com.stronggym.StrongGym.service.KorisnikService;

@Controller
@RequestMapping(value="/ClanskaKarta")
public class ClanskaKartaController {
	
	@Autowired KorisnikService korisnikService;
	@Autowired ClanskaKartaService clanskaService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}
	
	@GetMapping
	public ModelAndView index(
			
			HttpSession session, HttpServletResponse response) throws IOException {		
	
			ModelAndView rezultat = new ModelAndView("podnesiZahtev");
			
			return rezultat;
		
	}
	
	@PostMapping("/Create")
	public String create(HttpSession session,HttpServletResponse response,ModelMap map) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null ) {
			response.sendRedirect(baseURL+"login.html");
	
		}
		String error="";
		ClanskaKarta clanskaKarta=new ClanskaKarta(0,prijavljeniKorisnik,"0");
		List<ClanskaKarta> sve=clanskaService.findAll();
		boolean jednaki=false;
		for(ClanskaKarta clanskaKarta2:sve) {
			if(clanskaKarta2.getKorisnik().getId().equals(prijavljeniKorisnik.getId())) {
				jednaki=true;
				
			}
			
		}
		if (jednaki==true) {
			error="Vec ste podneli zahtev za clansku kartu,molimo sacekajte odobrenje za istu,bice Vam prikazano na profilu";
			map.put("poruka", error);
			return "podnesiZahtev";
		}
		else {
			
				clanskaService.save(clanskaKarta);
				System.out.println("Clanska kartica"+clanskaKarta);
				error="Uspesno ste podneli zahtev za loyalti karticu.Na profilu cete videti kada Vam bude odobrena.";
				map.put("poruka", error);
				return "podnesiZahtev";
				}
		
	
		
	}
	@GetMapping("/SveKartice")
	public ModelAndView prikaz(HttpSession session,HttpServletResponse response,ModelMap map) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator() ) {
			response.sendRedirect(baseURL+"login.html");
	
		}
		List<ClanskaKarta> clanskeKarte=clanskaService.findAll();
		ModelAndView rezultat=new ModelAndView("sveKartice");
		rezultat.addObject("clanskeKarte",clanskeKarte);
		
		return rezultat;
		
		
		
		
	}
	@GetMapping("/Details")
	public ModelAndView prikaz(@RequestParam Long id,HttpSession session,HttpServletResponse response,ModelMap map) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator() ) {
			response.sendRedirect(baseURL+"login.html");
	
		}
		ClanskaKarta clanskaKarta=clanskaService.findOne(id);
		System.out.println("jedna clanska"+clanskaKarta);
		ModelAndView rezultat=new ModelAndView("kartica");
		rezultat.addObject("clanskaKarta",clanskaKarta);
		
		return rezultat;
		
		
		
		
	}
	@PostMapping("/Edit")
	public void edit(@RequestParam Long id,@RequestParam int brojPoena,@RequestParam String popust,@RequestParam Boolean odobren,HttpSession session,HttpServletResponse response,ModelMap map) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator() ) {
			response.sendRedirect(baseURL+"login.html");
	
		}
		
		ClanskaKarta clanskaKarta=clanskaService.findOne(id);
		clanskaKarta.setBrojPoena(brojPoena);
		clanskaKarta.setPopust(popust);
		clanskaKarta.setOdobren(odobren);
		clanskaService.update(clanskaKarta);
		System.out.println("jedna clanska"+clanskaKarta);
		ModelAndView rezultat=new ModelAndView("kartica");
		rezultat.addObject("clanskaKarta",clanskaKarta);
		
		response.sendRedirect(baseURL+"ClanskaKarta/SveKartice");
		
		
		
		
	}

}

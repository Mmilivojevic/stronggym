package com.stronggym.StrongGym.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.stronggym.StrongGym.model.Komentar;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Slika;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.KomentarService;
import com.stronggym.StrongGym.service.KorisnikService;
import com.stronggym.StrongGym.service.SlikaService;
import com.stronggym.StrongGym.service.TerminService;
import com.stronggym.StrongGym.service.TipTreningaService;
import com.stronggym.StrongGym.service.TreningService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/Treninzi")
public class TreningController {
	
	@Autowired
	TreningService treningService;

	@Autowired
	KorisnikService korisnikService;

	@Autowired
	KomentarService komentarService;

	@Autowired
	TipTreningaService tipTreningaService;
	@Autowired
	TerminService terminService;
	@Autowired
	SlikaService slikaService;
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}

	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) String treneri,
			@RequestParam(required=false) Long tipId,
			
			@RequestParam(required=false) Double cenaOd,

			@RequestParam(required=false) Double cenaDo,
			@RequestParam(required=false) String vrstaTreninga,
			@RequestParam(required=false) String nivoTreninga,
					HttpSession session, HttpServletResponse response) throws IOException {		
		
		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		
		if(!prijavljeniKorisnik.isAdministrator()) {
			List<Trening> nepopunjeniTreninzi=treningService.nadjiNepopunjeneTreninge();
			System.out.println("Nepopunjeni treninzi"+ nepopunjeniTreninzi);
			List<TipTreninga> tipTreninga =  tipTreningaService.findAll();
			List<Slika> slike=slikaService.findAll();
			
			ModelAndView rezultat2 = new ModelAndView("treninzi");
			rezultat2.addObject("slike", slike);
			rezultat2.addObject("tipTreninga", tipTreninga);
			rezultat2.addObject("treninzi", nepopunjeniTreninzi);
			return rezultat2;
			
		}
		
		
		List<Trening> treninzi = treningService.find(naziv, tipId, cenaOd,cenaDo, treneri, vrstaTreninga, nivoTreninga);
		List<TipTreninga> tipTreninga =  tipTreningaService.findAll();
		List<Slika> slike=slikaService.findAll();
		System.out.println("Treninzi"+treninzi);
		ModelAndView rezultat = new ModelAndView("treninzi");
		rezultat.addObject("treninzi", treninzi);
		rezultat.addObject("slike", slike);
		rezultat.addObject("tipTreninga", tipTreninga);
	
		

		
		
		return rezultat;
	}
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Long id,HttpSession session,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		Trening trening =treningService.findOne(id);
		List<Termin> terminiTrening=terminService.findNepopunjeTermineZaJedanTrening(trening.getId());
		System.out.println("FInd one trening"+trening);
		List<Slika> slike=slikaService.findAll();
		System.out.println("Slike"+slike);
		List<TipTreninga> tipoviTreninga=tipTreningaService.findAll();
		Boolean zakazan=terminService.zakazanTermin(prijavljeniKorisnik.getId(), id);
		ModelAndView rezultat=new ModelAndView("trening");
		rezultat.addObject("trening",trening);
		rezultat.addObject("tipoviTreninga",tipoviTreninga);
		rezultat.addObject("terminiTrening",terminiTrening);
		rezultat.addObject("slike",slike);
		rezultat.addObject("zakazan",zakazan);
		return rezultat;
	}
	
	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session,HttpServletResponse response) throws IOException{
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Treninzi");
			return null;
		}
		//citanje
		List<TipTreninga> tipoviTreninga=tipTreningaService.findAll();
		List<Slika> slike=slikaService.findAll();
		//prosledjivanje na html
		ModelAndView rezultat=new ModelAndView("dodavanjeTreninga");
		rezultat.addObject("tipoviTreninga",tipoviTreninga);
		rezultat.addObject("slike",slike);
		return rezultat;
		
		
	}
	@PostMapping("/Create")
	public ModelAndView create(@RequestParam String naziv,@RequestParam String  treneri,@RequestParam String kratakOpis,@RequestParam Double cena ,@RequestParam String vrstaTreninga,@RequestParam String nivoTreninga,@RequestParam Double trajanjeTreninga,@RequestParam(name="slikaId", required=false) Long[] imageId,@RequestParam(name="tipId", required=false) Long[] tipIds,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Treninzi");
		}
		try {
			if (naziv.equals("")|| naziv==null ) {
				throw new Exception("Naziv ne sme biti prazan!");
			}
			if (treneri.equals("")|| treneri==null) {
				throw new Exception("Morate uneti trenere");
			}
			if (kratakOpis.equals("")||kratakOpis==null) {
				throw new Exception("Morate uneti kratak opis o knjizi");
			}
			if (cena==null) {
				throw new Exception("Cena ne moze biti nula");
			}
			if (trajanjeTreninga==null) {
				throw new Exception("Trajanje treninga ne moze biti nula");
			}
			if (vrstaTreninga.equals("")||vrstaTreninga==null) {
				throw new Exception("Morate odabrati vrstu treninga");
			}
			if (nivoTreninga.equals("")||nivoTreninga==null) {
				throw new Exception("Morate odabrati nivo treninga");
			}
			if (imageId==null||imageId.equals("")) {
				throw new Exception("Morate izabrati sliku");
			}
			if (tipIds==null||tipIds.equals("")) {
				throw new Exception("Morate izabrati tipove");
			}
			
			//kreiranje
			Trening trening=new Trening(naziv, treneri, kratakOpis, cena, vrstaTreninga, nivoTreninga, trajanjeTreninga);
			System.out.println("Trening "+trening);
			trening.setImagePath(slikaService.find(imageId));
			trening.setTipTreninga(tipTreningaService.find(tipIds));
			treningService.save(trening);
			response.sendRedirect(baseURL+"Treninzi");

			
			return null;
			}
			catch(Exception e){
				String poruka=e.getMessage();
				if(poruka=="") {
					poruka="Neuspesno kreiranje treninga";
				}
				List<TipTreninga> tipoviTreninga=tipTreningaService.findAll();
				List<Slika> slike=slikaService.findAll();
				//prosledjivanje u modelAndView
				ModelAndView rezultat=new ModelAndView("dodavanjeTreninga");
				rezultat.addObject("tipoviTreninga",tipoviTreninga);
				rezultat.addObject("slike",slike);
				rezultat.addObject("poruka",poruka);
			
			
				
				//				response.sendRedirect(baseURL+"Treninzi/Create");
				return rezultat;
				
			}
		

	}
	
	@PostMapping("/Edit")
	public ModelAndView edit(@RequestParam Long id,@RequestParam String naziv,@RequestParam String treneri,@RequestParam String kratakOpis,@RequestParam Double cena,@RequestParam String vrstaTreninga,@RequestParam String nivoTreninga,@RequestParam Double trajanjeTreninga,@RequestParam(name="slikaId",required=false) Long[] imageId,@RequestParam(name="tipId", required=false) Long[] tipIds,HttpSession session,HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		

		Trening trening=treningService.findOne(id);
		
		try {
			if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
				response.sendRedirect(baseURL+"Treninzi");
				return null;
			}
			if(trening==null) {
				response.sendRedirect(baseURL+"Treninzi");
				return null;
			}
			if (naziv.equals("")|| naziv==null ) {
				throw new Exception("Naziv ne sme biti prazan!");
			}
			if (treneri.equals("")|| treneri==null) {
				throw new Exception("Morate uneti trenere");
			}
			if (kratakOpis.equals("")||kratakOpis==null) {
				throw new Exception("Morate uneti kratak opis o knjizi");
			}
			if (cena==null) {
				throw new Exception("Cena ne moze biti nula");
			}
			if (trajanjeTreninga==null) {
				throw new Exception("Trajanje treninga ne moze biti nula");
			}
			if (vrstaTreninga.equals("")||vrstaTreninga==null) {
				throw new Exception("Morate odabrati vrstu treninga");
			}
			if (nivoTreninga.equals("")||nivoTreninga==null) {
				throw new Exception("Morate odabrati nivo treninga");
			}
			if (imageId==null||imageId.equals("")) {
				throw new Exception("Morate izabrati sliku");
			}
			if (tipIds==null||tipIds.equals("")) {
				throw new Exception("Morate izabrati tipove");
			}
			
			trening.setNaziv(naziv);
			System.out.println("Naziv "+naziv);
			trening.setTreneri(treneri);
			trening.setKratakOpis(kratakOpis);
			trening.setCena(cena);

			trening.setVrstaTreninga(vrstaTreninga);
			trening.setNivoTreninga(nivoTreninga);
			trening.setTrajanjeTreninga(trajanjeTreninga);
			trening.setImagePath(slikaService.find(imageId));
			System.out.println("Slika  "+imageId);
			trening.setTipTreninga(tipTreningaService.find(tipIds));
			
			
			treningService.update(trening);
			
			response.sendRedirect(baseURL+"Treninzi");


			
			return null;
			}
			catch(Exception e){
				String poruka=e.getMessage();
				if(poruka=="") {
					poruka="Neuspesno edit treninga";
				}
				List<TipTreninga> tipoviTreninga=tipTreningaService.findAll();
				List<Slika> slike=slikaService.findAll();
				//prosledjivanje u modelAndView
				ModelAndView rezultat=new ModelAndView("trening");
				rezultat.addObject("tipoviTreninga",tipoviTreninga);
				rezultat.addObject("slike",slike);
				rezultat.addObject("trening",trening);
				rezultat.addObject("poruka",poruka);
			
			
				
				return rezultat;
				
			}
		

		}

	@PostMapping(value="/DodavanjeKomentara")
	@ResponseBody
	public Map<String, Object> create(@RequestParam String tekst, @RequestParam String ocena, @RequestParam Long komentarisanTrening, @RequestParam Long autorKomentara, @RequestParam (required=false) Boolean anoniman, 
			HttpSession session, HttpServletRequest request,HttpServletResponse response) throws IOException {
	
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (loggedInUser == null || loggedInUser.isAdministrator()) {
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "odbijen");
			return odgovor;
		}

		Trening komentarisanTrening1 = treningService.findOne(komentarisanTrening);
		System.out.println("Komentarisan trening:"+komentarisanTrening1);
		
		Korisnik autorKomentara1 = korisnikService.findOne(autorKomentara);
		System.out.println("Autor komentara"+autorKomentara1);
		
		long yourmilliseconds = System.currentTimeMillis();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");    
	
		Date datumKomentara = new Date(yourmilliseconds);
		
		System.out.println(sdf.format(datumKomentara));
		
		
		try {
			

			if (tekst == null || tekst.equals("")) {
				throw new Exception("Polje za tekst ne sme biti prazno");
						
			}
			if (ocena == null || ocena.equals("")) {
				throw new Exception("Polje za ocenu ne sme biti prazno");
				
			}


			
			
			
			Komentar komentar = new Komentar(tekst, ocena, datumKomentara,"na cekanju",autorKomentara1,komentarisanTrening1,anoniman != null
					);
			System.out.println("Komentar za trening"+komentar);
			komentarService.save(komentar);
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "ok");
			response.sendRedirect(baseURL+"Treninzi");
			return odgovor;
			
			
		} catch (Exception ex) {
	
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neupesno!";
			}
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", poruka);
			return odgovor;
		}
		
		
	}
	@GetMapping(value="/SviKomentari")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public Map<String, Object> sviKomentaru(@RequestParam Long treningId, 
			HttpSession session) throws IOException {
	
		
		List<Komentar> komentari = komentarService.findAll(treningId);
		System.out.println("Svi komentari" + komentari);

		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("komentari", komentari);
		return odgovor;
	}
}

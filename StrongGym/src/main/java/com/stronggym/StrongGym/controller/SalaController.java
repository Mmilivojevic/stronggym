package com.stronggym.StrongGym.controller;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Sala;
import com.stronggym.StrongGym.model.Slika;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.SalaService;

@Controller
@RequestMapping(value="/Sala")
public class SalaController {

	@Autowired
	SalaService salaService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) String oznaka,
		
		
			HttpSession session, HttpServletResponse response) throws IOException {		
		// autentikacija, autorzacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL);
			return null;
		}

		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		if(oznaka!=null && oznaka.trim().equals(""))
			oznaka=null;
		
		
		
		// čitanje
	List<Sala> sale = salaService.findSala(oznaka);

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("sale");
		rezultat.addObject("sale", sale);
//
		return rezultat;
		
		
	}
	@PostMapping(value="/Edit")
	public void izmeniSalu(@RequestParam Long id,@RequestParam int kapacitet,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (loggedInUser == null || !loggedInUser.isAdministrator()) {
			response.sendRedirect(baseURL + "Treninzi");
			return;
		}
		
		Sala sala=salaService.findOne(id);
		sala.setKapacitet(kapacitet);
		salaService.update(sala);
		response.sendRedirect(baseURL+"/Sala");
	}
	@GetMapping(value="/Details")
	public ModelAndView detaljiSale(@RequestParam Long id,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		// samo administrator može da vidi druge korisnike; svaki korisnik može da vidi sebe
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator() ){
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}

		// validacija
		Sala sala = salaService.findOne(id);
	
		if (sala == null) {
			response.sendRedirect(baseURL + "Sala");
			return null;
		}

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("sala");
		rezultat.addObject("sala", sala);

		return rezultat;
	}
	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session,HttpServletResponse response) throws IOException{
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Treninzi");
			return null;
		}
		//citanje
	
		//prosledjivanje na html
		ModelAndView rezultat=new ModelAndView("dodavanjeSale");
	
		return rezultat;
		
		
	}
	@PostMapping("/Create")
	public ModelAndView create(@RequestParam String oznaka,@RequestParam int kapacitet,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Treninzi");
		}
		try {
			if (oznaka.equals("")|| oznaka==null ) {
				throw new Exception("Oznaka sale je obavezna!");
			}
			//odraditi da mi se iscitaju sve oznake sale i da proverim da li postoji vec
			//ako postoji ispisati drugu gresku
			if (kapacitet==0) {
				throw new Exception("kapacitet sale ne mozete biti 0");
			}
			
			
			//kreiranje
			Sala sala=new Sala(oznaka, kapacitet);
			salaService.save(sala);
			response.sendRedirect(baseURL+"Sala");

			
			return null;
			}
			catch(Exception e){
				String poruka=e.getMessage();
				if(poruka=="") {
					poruka="Neuspesno kreiranje sale";
				}
			
				//prosledjivanje u modelAndView
				ModelAndView rezultat=new ModelAndView("dodavanjeTreninga");
			
				rezultat.addObject("poruka",poruka);
			
			
				
				//				response.sendRedirect(baseURL+"Treninzi/Create");
				return rezultat;
				
			}
		

	}
	@PostMapping(value="/Delete")
	public void obrisi(@RequestParam Long id,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Treninzi");
		}
		Sala sala =salaService.findOne(id);
		if(sala.isZakazan()==true) {
			response.sendRedirect(baseURL+"Sala");
		}else {
		salaService.delete(id);
		response.sendRedirect(baseURL+"Sala");
		}
		
		
	}

}

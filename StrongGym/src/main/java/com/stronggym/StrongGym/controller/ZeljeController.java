package com.stronggym.StrongGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.model.Zelje;
import com.stronggym.StrongGym.service.KorisnikService;
import com.stronggym.StrongGym.service.TreningService;
import com.stronggym.StrongGym.service.ZeljeService;

@Controller
@RequestMapping(value="/Zelje")
public class ZeljeController {
	
	public static final String IZABRANi_TRENINZI_KORISNIK_KEY = "izabraniTreninziKorisnik";
	
private  String bURL; 
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private TreningService treningService;
	
	@Autowired
	private ZeljeService zeljeService;
	@Autowired
	private KorisnikService korisnikService;
	
	@PostConstruct
	public void init() {	
		
			bURL = servletContext.getContextPath()+"/";			
	}
	

	
	@GetMapping
	public ModelAndView nadjiZelje(
			@RequestParam Long korisnikId, 
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		
		
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KorisnikController.KORISNIK_KEY);
		
		if( korisnik.isAdministrator() == true) {
			response.sendRedirect(bURL+"Treninzi");
		}
		
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		
//		if (loggedInUser == null || (!loggedInUser.getKorisnickoIme().equals(korisnickoIme))) {
//			response.sendRedirect(bURL+"/Knjige");
//			return null;
//		}
		
		
		List<Zelje> zelje = zeljeService.findAll(korisnikId);
		
		
		// prosleđivanje
		
		ModelAndView rezultat = new ModelAndView("zelje");
		rezultat.addObject("zelje", zelje);
		
		
		
		return rezultat;
	}
	

	@PostMapping
	public ModelAndView dodajKnjiguUZelje(
			
			@RequestParam Long treningId,
			@RequestParam Long korisnikId,
			
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
	
//		
	Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KorisnikController.KORISNIK_KEY);		
//		if(korisnik==null || korisnik.isAdministrator()==true) {
//			response.sendRedirect(bURL+"/Trenizni");
//		}
		
//		Long id = new Long(knjigaId);
//		
//		if(trening!=null && id<=0) {
//			response.sendRedirect(bURL+"/Treninzi");
//			return null;
//		}
		System.out.println("Korisnik id"+ korisnikId);
		
		Trening trening = treningService.findOne(treningId);
//		Korisnik korisnik=korisnikService.findOne(korisnikId);
		System.out.println("Trening find one zelje"+trening);
		if(trening==null) {
			response.sendRedirect(bURL+"/Treninzi");
			return null;
		}
		
		Zelje zelje = new Zelje(korisnik, trening);
		zeljeService.save(zelje);
		
		List<Zelje> zeljeZaKorisnika = zeljeService.findAll(korisnik.getId());
		System.out.println("Trening find alll "+zeljeZaKorisnika);

		// prosleđivanje
		
		ModelAndView rezultat = new ModelAndView("zelje");
		rezultat.addObject("zelje", zeljeZaKorisnika);

		return rezultat;
	}

	@PostMapping(value="/Delete")
	public ModelAndView obrisiZelju(@RequestParam Long zeljeId, 	@RequestParam Long korisnikId,
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		// čitanje
	
			
		Zelje zelje = zeljeService.findOne(zeljeId);
		System.out.println("Zelje dele find one"+zelje);
		if(zelje==null) {
			
			System.out.println("asasdasdasdasdas");
			response.sendRedirect(bURL+"/Treninzi");
			
			return null;
		}
		
		zeljeService.delete(zeljeId);
			
		
//		
//		String korisnickoIme2=korisnickoIme.split(",")[0];
//		System.out.println("KORISNICKO IME"+korisnickoIme);
		List<Zelje> zelje2 = zeljeService.findAll(korisnikId);
	 System.out.println("Korpa 2 posle brisanja"+ zelje2);
//		
		ModelAndView rezultat = new ModelAndView("zelje");
		rezultat.addObject("zelje", zelje2);
		
		
		
		return rezultat;
	}	
	
}

			


package com.stronggym.StrongGym.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Sala;
import com.stronggym.StrongGym.model.Slika;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.SalaService;
import com.stronggym.StrongGym.service.TerminService;
import com.stronggym.StrongGym.service.TreningService;

@Controller
@RequestMapping(value="/Termini")
public class TerminController {
	@Autowired TreningService treningService;
	@Autowired SalaService salaService;
	@Autowired TerminService terminService;
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}
	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session,HttpServletResponse response) throws IOException{
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Treninzi");
			return null;
		}
		//citanje
		List<Trening> treninzi=treningService.findAll();
		List<Sala> sale=salaService.findAll();
		//prosledjivanje na html
		ModelAndView rezultat=new ModelAndView("dodavanjeTermina");
		rezultat.addObject("sale",sale);
		rezultat.addObject("treninzi",treninzi);
		return rezultat;
		
		
	}
	
	@PostMapping("/Create")
	public void create(@RequestParam Long treningId,@RequestParam Long  salaId,@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumTermina,@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.TIME) LocalTime vremeTermina,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Treninzi");
		}
//		try {
//			
//			}
//			
			//kreiranje
		LocalDateTime localDateTime=LocalDateTime.of(datumTermina,vremeTermina);
			Trening trening=treningService.findOne(treningId);
			Sala sala=salaService.findOne(salaId);
			
			Termin termin=new Termin(trening,sala, localDateTime);
			System.out.println("Trening "+trening);
			System.out.println("Termin"+termin);
			terminService.save(termin);
			response.sendRedirect(baseURL+"Treninzi");

			
		
//			}
//			catch(Exception e){
//				String poruka=e.getMessage();
//				if(poruka=="") {
//					poruka="Neuspesno kreiranje treninga";
//				}
//				List<TipTreninga> tipoviTreninga=tipTreningaService.findAll();
//				List<Slika> slike=slikaService.findAll();
//				//prosledjivanje u modelAndView
//				ModelAndView rezultat=new ModelAndView("dodavanjeTreninga");
//				rezultat.addObject("tipoviTreninga",tipoviTreninga);
//				rezultat.addObject("slike",slike);
//				rezultat.addObject("poruka",poruka);
//			
//			
//				
//				//				response.sendRedirect(baseURL+"Treninzi/Create");
//				return rezultat;
//				
//			}
		

	}

}

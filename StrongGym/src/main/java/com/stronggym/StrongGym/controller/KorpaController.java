package com.stronggym.StrongGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TerminKorisnik;
import com.stronggym.StrongGym.model.TipTreninga;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.service.KorisnikService;
import com.stronggym.StrongGym.service.TerminKorisnikService;
import com.stronggym.StrongGym.service.TerminService;
import com.stronggym.StrongGym.service.TipTreningaService;
import com.stronggym.StrongGym.service.TreningService;

@Controller
@RequestMapping(value="/Korpa")
public class KorpaController {
	public static final String IZABRANI_TRENINZI_KORISNIK_KEY = "izabraniTreninziKorisnik";
	
	
	@Autowired
	private TreningService treningService;
	@Autowired
	private TerminService terminService;
	@Autowired
	private TipTreningaService tipTreningaService;
	
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private TerminKorisnikService terminKorisnikService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}
	
	@GetMapping
	public ModelAndView index(
			
			HttpSession session, HttpServletResponse response) throws IOException {		
	
			ModelAndView rezultat = new ModelAndView("korpa");
			
			return rezultat;
		
	}
	@GetMapping(value="/Details")
	@SuppressWarnings("unchecked")
	public ModelAndView detaljiKorpa(@RequestParam Long id, 
			HttpSession session, HttpServletResponse response) throws IOException {
		
		
		Termin termin=terminService.findOne(id);
		System.out.println("Trening detalji korpa" + termin);

		List<Termin> odabrana = (List<Termin>) session.getAttribute(KorpaController.IZABRANI_TRENINZI_KORISNIK_KEY);
		System.out.println("ODABRANA"+odabrana);
		boolean isti=false;
		if(!odabrana.isEmpty()) {
			
			for(Termin termin2:odabrana) {
				
				if(termin2.getId().equals(termin.getId())) {
					isti=true;
					break;
				}
				System.out.println("Termin 2"+termin2);
			}
		}
		
		
		if(isti==true) {
			
			List<TipTreninga> tipTreninga=tipTreningaService.findByTreningId(termin.getTrening().getId());
			System.out.println("TIPOVI"+tipTreninga);
			
			ModelAndView rezultat = new ModelAndView("korpa");
			rezultat.addObject("tipovi",tipTreninga);
			return rezultat;
			
			
		}else {
			odabrana.add(termin);
			List<TipTreninga> tipTreninga=tipTreningaService.findByTreningId(termin.getTrening().getId());
			System.out.println("TIPOVI"+tipTreninga);
			
			ModelAndView rezultat = new ModelAndView("korpa");
			rezultat.addObject("tipovi",tipTreninga);
			return rezultat;
		}
		
//		if (!odabrana.iterator().next().getId().equals(termin.getId())) {
//			System.out.println("Termin get id"+termin.getId());
//			odabrana.add(termin);
//			System.out.println("odabrana"+odabrana);
//		}
//		System.out.println("LONG ID ZA JEDNOG"+id);
//		System.out.println("Odbabrana" + odabrana);
	
		
	}
	

	@PostMapping(value="/Delete")
	@SuppressWarnings("unchecked")
	public ModelAndView obrisiKnjiguIzKorpe(@RequestParam Long id, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Termin termin=terminService.findOne(id);

		List<Termin> odabrana = (List<Termin>) session.getAttribute(KorpaController.IZABRANI_TRENINZI_KORISNIK_KEY);
		boolean isti=false;
		if(!odabrana.isEmpty()) {
			
			for(Termin termin2:odabrana) {
				
				if(termin2.getId().equals(termin.getId())) {
					isti=true;
					break;
				}
				System.out.println("Termin 2"+termin2);
			}
		}
		if(isti==true) {
			
			odabrana.remove(termin); 
		
			ModelAndView rezultat = new ModelAndView("korpa");
			System.out.println("ODABRANA"+odabrana);
			return rezultat;
		}
		else {
			
			List<TipTreninga> tipTreninga=tipTreningaService.findByTreningId(termin.getTrening().getId());
			System.out.println("TIPOVI"+tipTreninga);
			
			ModelAndView rezultat = new ModelAndView("korpa");
			rezultat.addObject("tipovi",tipTreninga);
			return rezultat;
		}
		
		
	}
	@PostMapping(value="/Kupi")
	public ModelAndView kupiTrening(@RequestParam Long korisnikId,@RequestParam Long id,HttpServletResponse response,HttpSession session) throws IOException {
		Termin termin=terminService.findOne(id);
		Korisnik korisnik=korisnikService.findOne(korisnikId);
		List<TerminKorisnik> terminiKorisnici=terminKorisnikService.findAll();
		String error="";
		for(TerminKorisnik terminKorisnik2:terminiKorisnici) {
			if(terminKorisnik2.getTermin().getId().equals(id) || terminKorisnik2.getKorisnik().getId().equals(korisnikId)) {
				error="Zakazali ste ovaj trening vec";
			}
		}
		TerminKorisnik terminKorisnik=new TerminKorisnik(termin,korisnik);
		terminKorisnikService.save(terminKorisnik);
		String korisnickoIme=korisnik.getKorisnickoIme();
		response.sendRedirect(baseURL+"Korisnik/Profil?korisnickoIme="+korisnickoIme);
		
		
		return null;
	}

}

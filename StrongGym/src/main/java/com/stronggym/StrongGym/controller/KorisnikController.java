package com.stronggym.StrongGym.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.stronggym.StrongGym.model.ClanskaKarta;
import com.stronggym.StrongGym.model.Korisnik;
import com.stronggym.StrongGym.model.Termin;
import com.stronggym.StrongGym.model.TerminKorisnik;
import com.stronggym.StrongGym.model.Trening;
import com.stronggym.StrongGym.model.Zelje;
import com.stronggym.StrongGym.service.ClanskaKartaService;
import com.stronggym.StrongGym.service.KorisnikService;
import com.stronggym.StrongGym.service.TerminKorisnikService;
import com.stronggym.StrongGym.service.TerminService;
import com.stronggym.StrongGym.service.TreningService;
import com.stronggym.StrongGym.service.ZeljeService;

@Controller
@RequestMapping(value="/Korisnik")
public class KorisnikController {
	

	@Autowired
	KorisnikService korisnikService;
	@Autowired
	TerminService terminService;
	@Autowired
	ZeljeService zeljeService;
	@Autowired
	TreningService treningService;
	@Autowired
	TerminKorisnikService terminKorisnikService;
	@Autowired ClanskaKartaService clanskaKartaService;

	public static final String KORISNIK_KEY = "prijavljeniKorisnik";
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}

	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) String korisnickoIme,
			@RequestParam(required=false) Boolean administrator,
			
		
			HttpSession session, HttpServletResponse response) throws IOException {		
		// autentikacija, autorzacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL);
			return null;
		}

		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		if(korisnickoIme!=null && korisnickoIme.trim().equals(""))
			korisnickoIme=null;
		
		
		
		// čitanje
	List<Korisnik> korisnici = korisnikService.find(korisnickoIme,  administrator);

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", korisnici);
//
		return rezultat;
	}
	@PostMapping(value="/Register")
	public ModelAndView register(@RequestParam String korisnickoIme, @RequestParam String lozinka,  @RequestParam String ponovljenaLozinka,@RequestParam String email,
			 @RequestParam String ime, @RequestParam String prezime,@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date datumRodjenja,  @RequestParam String adresa, @RequestParam String brojTelefona,
			HttpSession session, HttpServletResponse response) throws IOException {
		try {
			// validacija
			Korisnik postojeciKorisnik = korisnikService.findOne(korisnickoIme);
			if (postojeciKorisnik != null) {
				throw new Exception("Korisničko ime već postoji!");
			}
			if (korisnickoIme.equals("") || lozinka.equals("")) {
				throw new Exception("Korisničko ime i lozinka ne smeju biti prazni!");
			}
			if (!lozinka.equals(ponovljenaLozinka)) {
				throw new Exception("Lozinke se ne podudaraju!");
			}
			if (email.equals("")) {
				throw new Exception("Emejl ne moze biti prazno polje!");
			}
			if (ime.equals("")) {
				throw new Exception("Ime ne sme biti prazno!");
			}
			if (prezime.equals("")) {
				throw new Exception("Prezime ne sme biti prazno!");
			}
			if (datumRodjenja.equals("")) {
				throw new Exception("Datum rodjenja ne sme biti prazno polje!");
			}
			if (adresa.equals("")) {
				throw new Exception("Adresa ne sme biti prazno polje!");
			}
			if (brojTelefona.equals("")) {
				throw new Exception("Broj telefona ne sme biti prazno polje!");
			}

//			
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");    
//		
			Date datumRegistracije = new Date();
			
	
			// registracija
			Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumRegistracije);
			korisnikService.save(korisnik);

			System.out.println("Korisnik save:"+korisnik);
			response.sendRedirect(baseURL + "login.html");
			return null;
		} catch (Exception ex) {
			// ispis greške
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna registracija!";
			}

			// prosleđivanje
			ModelAndView rezultat = new ModelAndView("registracija");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}


	@SuppressWarnings("null")
	@PostMapping(value="/Login")
	public String postLogin(@RequestParam String korisnickoIme, @RequestParam String lozinka, 
			HttpSession session, HttpServletResponse response,ModelMap map) throws IOException {
		
		
//		try {
//			
//			//validacija
//			Korisnik korisnik=korisnikService.findBlokiranogKorisnika(korisnickoIme, lozinka);
//			System.out.println("Korisnik za logovanje "+korisnik);
//			if(korisnik==null && korisnik.isBlock()==true) {
//				throw new Exception("Neispravno korisnicko ime ili lozinka");
//		
//			}
//			//prijava
//			session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
//			response.sendRedirect(baseURL+"Treninzi");
//			return null;
//		}catch(Exception ex) {
//			//ispis greske
//			String poruka=ex.getMessage();
//			if(poruka=="") {
//				poruka="Neuspesna prijava!";
//				
//			}
//			//prosledjivanje
//			ModelAndView rezultat=new ModelAndView("prijava");
//			rezultat.addObject("poruka",poruka);
//			return rezultat;
//		}
	
		
		String error="";
			// validacija
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		
			Korisnik korisnik = korisnikService.findBlokiranogKorisnika(korisnickoIme, lozinka);
			if(loggedInUser !=null) {
				error="Vec ste prijavljeni!";
				map.put("error",error);
				return "prijava";
			}
			if (korisnik == null) {
				error="Neispravno korisničko ime ili lozinka!";
				map.put("error",error);
				return "prijava";
			}if(korisnik.isBlock()==true){
		error="Blokirani ste od strane administratora!";
		map.put("error",error);
		return "prijava";
	}
			

			// prijava
			session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
			
			response.sendRedirect(baseURL+"Treninzi");
			
		return "treninzi";
		
	}
	@GetMapping(value="/Profil")
	public ModelAndView profilKorisnika(@RequestParam String korisnickoIme, 
			HttpSession session, HttpServletResponse response) throws IOException {		
		
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		
		
		if (loggedInUser == null || (!loggedInUser.getKorisnickoIme().equals(korisnickoIme))) {
			response.sendRedirect(baseURL);
			return null;
		}

	
		Korisnik korisnik = korisnikService.findOne(loggedInUser.getKorisnickoIme());
		if (korisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		List<Zelje> zelje=zeljeService.findAll(korisnik.getId());
		List<TerminKorisnik> terminKorisnik=terminKorisnikService.findAllByKorisnikId(korisnik.getId());
		System.out.println("korisnik za termine"+ korisnik.getId());
		
		System.out.println("Korisnik za profil korisnika   :"+korisnik);
		ClanskaKarta clanskaKarta=clanskaKartaService.findOneByKorisnikId(korisnik.getId());
	
		ModelAndView rezultat = new ModelAndView("profil");
		rezultat.addObject("korisnik", korisnik);
		rezultat.addObject("zelje", zelje);
		rezultat.addObject("terminKorisnik", terminKorisnik);
		rezultat.addObject("clanskaKarta", clanskaKarta);

		return rezultat;
	}
	@PostMapping(value="/EditObican")
	public void izmeniKorisnikaObican(@RequestParam String korisnickoIme,@RequestParam (required=false)String lozinka,@RequestParam (required=false)String ponovljenaLozinka,@RequestParam String email,@RequestParam String ime,@RequestParam String prezime,@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date datumRodjenja,@RequestParam String adresa,@RequestParam String broj,@RequestParam  @DateTimeFormat(pattern = "yyyy-MM-dd") Date datumRegistracije,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (loggedInUser == null || loggedInUser.isAdministrator()) {
			response.sendRedirect(baseURL + "Treninzi");
			return;
		}
		System.out.println("Korisnicko ime za korisnika:"+korisnickoIme);
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		System.out.println("korisnik obicna za edit"+ korisnik);
		korisnik.setKorisnickoIme(korisnickoIme);
		if(lozinka=="" && ponovljenaLozinka=="") {
			korisnik.setLozinka(korisnik.getLozinka());
		}
		if(lozinka!="" || ponovljenaLozinka!="") {
			if(lozinka.equals(ponovljenaLozinka)) {
				korisnik.setLozinka(lozinka);
			}
		}
		korisnik.setEmail(email);
		korisnik.setIme(ime);
		korisnik.setPrezime(prezime);
		korisnik.setAdresa(adresa);
		korisnik.setBrojTelefona(broj);
		korisnik.setDatumRegistracije(datumRegistracije);
		korisnik.setDatumRodjenja(datumRodjenja);
		korisnikService.update2(korisnik);
		response.sendRedirect(baseURL+"Treninzi");
		
	
	
	}
	@PostMapping(value="/Edit")
	public void izmeniKorisnika(@RequestParam String korisnickoIme, @RequestParam (required=false) boolean administrator, @RequestParam(required=false) boolean block,
			  HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (loggedInUser == null || !loggedInUser.isAdministrator()) {
			response.sendRedirect(baseURL + "Treninzi");
			return;
		}
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		
	
		 
		
			korisnik.setAdministrator(administrator);
		
	
	
			
			korisnik.setBlock(block);
			korisnikService.update(korisnik);
		
		
		
		response.sendRedirect(baseURL + "Korisnik");
		
	}
	
	@GetMapping(value="/Details")
	public ModelAndView detaljiKorisnika(@RequestParam String korisnickoIme,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		// samo administrator može da vidi druge korisnike; svaki korisnik može da vidi sebe
		if (prijavljeniKorisnik == null || (!prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.getKorisnickoIme().equals(korisnickoIme))) {
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}

		// validacija
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		System.out.println("Korisnik find one"+ korisnik);
		if (korisnik == null) {
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject("korisnik", korisnik);

		return rezultat;
	}
	@PostMapping(value="/DeleteZakazan")
	public void obrisiZakazan( @RequestParam Long korisnikId,@RequestParam Long terminId,HttpSession session,HttpServletResponse response) throws IOException {
		Termin termin=terminService.findOne(terminId);
		Korisnik korisnik=korisnikService.findOne(korisnikId);
		if(termin!=null || korisnik !=null) {
			terminKorisnikService.delete(terminId, korisnikId);
		}
		response.sendRedirect(baseURL+"Korisnik/Profil?korisnickoIme="+korisnik.getKorisnickoIme());
		
	}
	@PostMapping(value="/DeleteTermin")
	public void obrisiZakazan( @RequestParam Long terminId,HttpSession session,HttpServletResponse response) throws IOException {
		Trening trening=treningService.findOne(terminId);
		Korisnik korisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(trening!=null) {
			zeljeService.delete(trening.getId(), korisnik.getId());
		}
		response.sendRedirect(baseURL+"Korisnik/Profil?korisnickoIme="+korisnik.getKorisnickoIme());
		
	}
	

	@GetMapping(value="/Logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		// odjava	
		session.invalidate();
		
		response.sendRedirect(baseURL+"login.html");
	}

}
